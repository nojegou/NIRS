# -*- coding: utf-8 -*-
"""
Created on Mon Jun 26 14:04:40 2023

# Authors: Nolwenn Jegou <nolwenn.jegou@inria.fr>
"""

import unittest 
import FRESHpipeline_FirstLevelProcessing
import mne_nirs
from numpy import array


class TestFRESH_FirstLevelProcessing(unittest.TestCase):
    
    
    def test_padded_SNIRF(self):
        #reproduce list of RawSNIRF
        datalist=list()
        for i in range(3):
            data = mne_nirs.simulation.simulate_nirs_raw(sig_dur=1)
            data._data=array([[i,i,i]])
            datalist.insert(i,data)
        paddedtest=FRESHpipeline_FirstLevelProcessing.padded_SNIRF(datalist,3)
        self.assertTrue((paddedtest.get_data()==array([[0,0,0,1,1,1,2,2,2]])).all())
        self.assertRaises(Exception,paddedtest,datalist,2)
        
        
if __name__ == '__main__':
    unittest.main()