# -*- coding: utf-8 -*-
"""
Created on Wed Jun 21 15:32:17 2023

# Authors: Nolwenn Jegou <nolwenn.jegou@inria.fr>
"""
import mne_bids
import mne_nirs
import mne
import matplotlib.pyplot as plt
import matplotlib.collections as collections
import numpy as np

BidsPath="C:/Users/nojegou/Documents/Data/DemianAcquisition"

##WIP

def plotGrpAvgcombined(datahbo,datahbr,time,title) :
    """

    Parameters
    ----------
    datahbo : TYPE
        DESCRIPTION.
    datahbr : TYPE
        DESCRIPTION.
    time : TYPE
        DESCRIPTION.
    title : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    
    #for each line of hbo need to be substract from mean
    cleandatahbo=datahbo
    cleandatahbr=datahbr
    """
    i=0
    for epoch in datahbo :
        cleandatahbo[i]=epoch-np.mean(epoch)
        i=i+1
    i=0
    for epoch in datahbr :
        cleandatahbr[i]=epoch-np.mean(epoch)
        i=i+1  
   """
   
    stduphbo=np.mean(cleandatahbo,axis=0)+np.std(cleandatahbo,axis=0)
    stddownhbo=np.mean(cleandatahbo,axis=0)-np.std(cleandatahbo,axis=0)
    meanhbo=np.mean(cleandatahbo,axis=0)
    stduphbr=np.mean(cleandatahbr,axis=0)+np.std(cleandatahbr,axis=0)
    stddownhbr=np.mean(cleandatahbr,axis=0)-np.std(cleandatahbr,axis=0)
    meanhbr=np.mean(cleandatahbr,axis=0)
    cihbo=1.95 * np.std(cleandatahbo,axis=0) / np.sqrt(len(cleandatahbo))
    cihbr=1.95 * np.std(cleandatahbr,axis=0) / np.sqrt(len(cleandatahbr))
    
    plt.plot(time[0:len(meanhbo)], meanhbo, color='tomato')
    plt.plot(time[0:len(meanhbr)], meanhbr, color='royalblue')
    plt.legend(["HbO", "HbR"])
    plt.fill_between(time[0:len(meanhbo)], meanhbo+cihbo, meanhbo-cihbo, alpha=.5, linewidth=0, color='tomato')
    plt.fill_between(time[0:len(meanhbr)], meanhbr+cihbr ,meanhbr-cihbr, alpha=.5, linewidth=0, color='royalblue')
    plt.axvspan(0,18, facecolor='#2ca02c', alpha=.2,color='k')
    plt.title(title)
    plt.ylim([-5e-7,6e-7])
    plt.show()
    
    
    
    
    
dataset = mne_bids.BIDSPath(root=BidsPath, task=mne_bids.get_entity_vals(BidsPath, 'task')[0], datatype="nirs", suffix="nirs", extension=".snirf")
subjects = mne_bids.get_entity_vals(BidsPath, 'subject')
sessions=mne_bids.get_entity_vals(BidsPath,'session')
if (sessions==[]): sessions=[None]
bids_path=dataset.update(subject=subjects[0], session=sessions[0])
raw_data = mne_bids.read_raw_bids(bids_path=bids_path, extra_params=None, verbose=True)
coordinates = mne.preprocessing.nirs.beer_lambert_law(mne.preprocessing.nirs.optical_density(raw_data)).get_montage() #coordinates to plot result at the right space
annotations=set(raw_data.annotations.description) 
del raw_data

