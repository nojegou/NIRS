# -*- coding: utf-8 -*-
"""
Created on Tue Apr 19 14:21:04 2022

# Authors: Nolwenn Jegou <nolwenn.jegou@inria.fr>
#
"""
import argparse
import textwrap
import mne
import mne_nirs
import mne_bids
import pandas as pd
import numpy as np
from itertools import compress
from os import path, makedirs, remove
import datetime
from pickle import dump

class Pipeline:
    """Class that carries pipeline parameters : bids_path, quality_check_option, filter_param, drift_model, drift_hp, drift_order, glm_model, glm_noise_model)"""

    def __init__(self, bids_path, quality_check_option, crop, trigger_length, filter_param, drift_model, drift_hp, drift_order, glm_model, glm_noise_model, excel):
        self.bids_path = bids_path
        self.quality_check_option = quality_check_option
        self.crop=crop
        self.trigger_length = trigger_length
        self.filter_param = filter_param    
        self.drift_model = drift_model
        self.drift_hp = drift_hp
        self.drift_order = drift_order
        self.glm_model = glm_model
        self.glm_noise_model = glm_noise_model
        self.sessions = None
        self.task_names = None
        self.excel = excel

    def get_bids_path(self):
        return self.bids_path
    
    def get_quality_check_option(self):
        return self.quality_check_option
    
    def get_crop(self):
        return self.crop
    
    def get_trigger_length(self):
        return self.trigger_length
    
    def get_filter_param(self):
        return self.filter_param
    
    def get_drift_model(self):
        return self.drift_model
    
    def get_drift_hp(self):
        return self.drift_hp
    
    def get_drift_order(self):
        return self.drift_order
    
    def get_glm_model(self):
        return self.glm_model
    
    def get_glm_noise_model(self):
        return self.glm_noise_model
    
    def set_glm_noise_model(self, new_noise_model):
        self.glm_noise_model = new_noise_model;
    
    def set_sessions(self, sessions_list):
        self.sessions = sessions_list
    
    def set_task_names(self, task_list):
        self.task_names = task_list
    
    def get_sessions(self):
        return self.sessions
    
    def get_task_names(self):
        return self.task_names
        
    def get_excel(self):
        return self.excel
    
    def to_string(self):
        """Get parameters to string"""
        return "First level analysis parameters.\nDatapath = "+self.get_bids_path()+"\nDate and time = "+str(datetime.datetime.now())+"\nCrop = "+str(self.get_crop())+"\nfilter cutoff frequency  = ["+str(self.get_filter_param())+"]\nGLM drift model = "+str(self.get_drift_model())+"\nDrift high pass = "+str(self.get_drift_hp())+"\nDrift order = "+str(self.get_drift_order())+"\nGlm model = "+self.get_glm_model()+"\nGlm noise model = "+self.get_glm_noise_model()+"\nExcel version = "+str(self.get_excel())
    

def generalized_design_matrix (design_matrix, boolSSD, short_channels, sessionNumber, session):
    """
    Combinate design matrix (DM) of every session with short channels if detected, each design matrix has two short channel regression, mean of HbO and HbR. 
    generalizedDM = id * [DM(0),...,DM(i)].T
   
    Parameters
    ----------
    design_matrix : Dataframe
        Regressor of signals.
    boolSSD : bool
        Presence of short chanels in the data.
    short_channels : RawSNIRF
        List of short channels if they are present in the data.
    sessionNumber : int
        Number of sessions per subject.
    session : List of String
        List of session names.

    Returns
    -------
    generalizedDM : Dataframe
        Generalized Design Matrix, the Design Matrix of each sessions is convolved with the identity matrix.

    """
    indexDM = list()
    lenxDM = [0]
    lenyDM = [0]
    
    
    for i in range(sessionNumber): 
        #Get if hbo and hbr short channels are given the mean of both should be add separatly if not the mean of the given is added.
        if(boolSSD):
            hbo=[chn for chn in short_channels[0].ch_names if 'hbo' in chn]
            hbr=[chn for chn in short_channels[0].ch_names if 'hbr' in chn]
            if (hbo!=[] and hbr!=[]): # add mean of hbo and hbr short channels to design matrix
                design_matrix[i]["MeanShortHbO"] = np.mean(short_channels[i].copy().pick(picks="hbo").get_data(), axis = 0)
                design_matrix[i]["MeanShortHbR"] = np.mean(short_channels[i].copy().pick(picks="hbr").get_data(), axis = 0)
            elif (hbo!=[]):
                design_matrix[i]["MeanShortHbO"] = np.mean(short_channels[i].copy().pick(picks="hbo").get_data(), axis = 0)
            elif (hbr!=[]):
                design_matrix[i]["MeanShortHbR"] = np.mean(short_channels[i].copy().pick(picks="hbr").get_data(), axis = 0)
        lenxDM.append(lenxDM[i]+len(design_matrix[i].T))
        lenyDM.append(lenyDM[i]+len(design_matrix[i]))
        
    
        if (sessionNumber == 1): indexDM+=[e for e in design_matrix[i].keys().to_list()]
        else : indexDM += [session[i]+"-"+e for e in design_matrix[i].keys().to_list()]
        
        
    # generalize design matrix : combination of the individual ones to create a single big one
    generalizedDM = np.zeros((lenyDM[-1],lenxDM[-1]))
    for j in range(len(design_matrix)):
        generalizedDM[lenyDM[j]:lenyDM[j+1],lenxDM[j]:lenxDM[j+1]] = design_matrix[j]
    
    
    generalizedDM = pd.DataFrame(generalizedDM, columns=indexDM)   # generalized design matrix to dataframe
    
    
    # visualize design matrix
    # import nilearn.plotting
    # nilearn.plotting.plot_design_matrix(generalizedDM)
    
    
    return generalizedDM
    


def padded_SNIRF(SNIRFlist, sessionNumber):
    """
    List of RawSNIRF data per session are put in one RawSNIRF data one after the other

    Parameters
    ----------
    SNIRFlist : list[RawSNIRF]
        List of RawSNIRF data, each element of the list correspond to one session.
    sessionNumber : int
        Number of sessions per subject.

    Returns
    -------
    paddedSNIRF : RawSNIRF
        Sessions are put one after the other for each channels.

    """
    if sessionNumber!=len(SNIRFlist):
        raise Exception("Number of session given does not match data") 
    #padded data = every session one after the other
    paddedSNIRF = SNIRFlist[0]
    for sessnum in range(1,sessionNumber):
        paddedSNIRF.append([SNIRFlist[sessnum]])
    return paddedSNIRF    


def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]   
    
def quality_check(OD, raw_data, BPfilt, quality_check_option, pipeline):
    """
    Quality check of session with scalp coupling index (SCI), peak spectral power (PSP) and signal to noise ratio (SNR). 

    Parameters
    ----------
    OD : RawSNIRF
        Optical density, raw NIRS signal converted to optical density.
    raw_data : RawSNIRF
        Raw NIRS signal.
    BPfilt : RawSNIRF
        Concentration changes, optical density converted to concentration changes and filtered depending on pipeline options.
    quality_check_option : string
        Quality check option chosen.

    Returns
    -------
    bad_channels : numpy.array
        Channels that don't pass the desired quality check

    """
    
    if quality_check_option == 'sci-psp':
        sci = mne.preprocessing.nirs.scalp_coupling_index(OD) #scalp coupling index calculation
        psp = mne_nirs.preprocessing.peak_power(OD,time_window = raw_data.times[-1]) #psp calculation
        sci_bads = list(compress(raw_data.ch_names, sci < 0.8)) #list of boolean, if true the channel passes the sci test
        psp_bads = list(compress(raw_data.ch_names, psp[1] < 0.1)) #list of boolean, if true the channel passes the psp test
        bad_channels = list(set(sci_bads + psp_bads)) #Channels that don't pass both tests arebcassified as "bad" channels
    elif quality_check_option == 'snr':
        snr=np.empty(len(raw_data.get_data()[:,0])) # signal to noise ratio 
        for j,signal in enumerate(raw_data.get_data()):
            snr[j] = (signal.mean()/signal.std())
        bad_channels = list(compress(raw_data.ch_names, snr < 8))
    elif quality_check_option == 'correlate':
        
        
        #get part of signal in triggers
        triggerlist=list()
        for time_trigger in BPfilt.annotations.onset:
            trigger=np.where(BPfilt.times==time_trigger)[0][0]
            if pipeline.get_trigger_length()==None :
                trigger_length=(np.where(BPfilt.times==find_nearest(BPfilt.times,time_trigger+BPfilt.annotations.duration[0]))-trigger)[0][0]
            else: 
                trigger_length=pipeline.get_trigger_length()
            triggerlist.append(BPfilt.get_data()[:,trigger:trigger+trigger_length])
            
            
            #get block average for every channel
            block_average=np.zeros((len(BPfilt.ch_names),trigger_length))
            for triggerblock in triggerlist:
                block_average=block_average+triggerblock
            block_average=block_average/len(BPfilt.annotations.onset)
            
            
            #get correlation between hbo and hbr channels
            correlation=np.zeros(len(block_average))
            uncorrelatedchannels=np.zeros(len(block_average))
            for i in range(0,len(block_average),2):
                normalizedhbo=block_average[i,:] / 1e-8
                normalizedhbr=block_average[i+1,:] / 1e-8
                correlation[i:i+2]=np.correlate(block_average[i,:],block_average[i+1,:])
        bad_channels = list(compress(raw_data.ch_names, correlation > 0))
    else : bad_channels = list()
    return bad_channels
        

def run_glm(session, sessionNumber, boolSSD, data, pipeline):
    """
    Run GLM separately on hbo and hbr depending on the glm model parameter.

    Parameters
    ----------
    session : List[String]
        List od session names.
    sessionNumber : Int
        number os sessions.
    boolSSD :  RawSNIRF
        True of there is short channels in data.
    data :  RawSNIRF
        NIRS data.
    pipeline : Pipeline
        Pipeline class which stores pipeline parameters.

    Returns
    -------
    glm_est : Dataframe
        Dataframe Two-dimensional tabular data.

    """
    design_matrix = list()
    long_channels = list()
    short_channels = list()
    
    if(pipeline.get_glm_noise_model()=='arN'):
        pipeline.set_glm_noise_model('ar'+str(int(data[0].info['sfreq']*4)))  #Based on Nilearn internal recommandations                                                                        
    #depending on pipeline parameters the chromophore are calculates separately or simultaneousely
    if (pipeline.get_glm_model()=='gen-separate'):
        for chromo in ['hbo','hbr']:
            for sessnum in range(sessionNumber):
                
                #separate short from long channels
                if(boolSSD):
                    short_channels.append(mne_nirs.channels.get_short_channels(data[sessnum].copy().pick([chromo])))
                else : short_channels=[]
                long_channels.append(mne_nirs.channels.get_long_channels(data[sessnum].copy().pick([chromo])))
                design_matrix.append(mne_nirs.experimental_design.make_first_level_design_matrix(long_channels[sessnum],drift_model=pipeline.get_drift_model(), drift_order=pipeline.get_drift_order(), high_pass=pipeline.get_drift_hp(), hrf_model='SPM', stim_dur=data[sessnum].annotations.duration[0]))   
       
        #creating a disign matrix per chromophore with l
        generalizedDMDFhbo = generalized_design_matrix(design_matrix[0:sessionNumber], boolSSD, short_channels[0:sessionNumber], sessionNumber, session)
        generalizedDMDFhbr = generalized_design_matrix(design_matrix[sessionNumber:], boolSSD, short_channels[sessionNumber:], sessionNumber, session)
        
        
        paddedBPfilt = padded_SNIRF(data, sessionNumber)
        
        
        glm_est_hbo = mne_nirs.statistics.run_glm(paddedBPfilt.copy().pick(['hbo']), generalizedDMDFhbo, noise_model=pipeline.get_glm_noise_model()) # Run GLM
        glm_est_hbr = mne_nirs.statistics.run_glm(paddedBPfilt.copy().pick(['hbr']), generalizedDMDFhbr, noise_model=pipeline.get_glm_noise_model()) # Run GLM
        
        
        glm_DF=glm_est_hbo.to_dataframe()
        glm_DF_hbr=glm_est_hbr.to_dataframe()
        
        
        rejectedhbr, corr_p_valhbr=mne.stats.fdr_correction(glm_DF_hbr.p_value)   # fdr correction of hbr p_values
        rejectedhbo, corr_p_valhbo=mne.stats.fdr_correction(glm_DF.p_value)   # fdr correction of hbo p_values
        
        
        glm_DF["fdr_p_value"]=corr_p_valhbo
        glm_DF_hbr["fdr_p_value"]=corr_p_valhbr
        glm_DF=glm_DF.merge(glm_DF_hbr, how='outer')
        
            
    elif (pipeline.get_glm_model()=='gen-same'):
        for sessnum in range(sessionNumber):
            short_channels.append(mne_nirs.channels.get_short_channels(data[sessnum].copy()))
            long_channels.append(mne_nirs.channels.get_long_channels(data[sessnum].copy()))
            design_matrix.insert(sessnum,mne_nirs.experimental_design.make_first_level_design_matrix(long_channels[sessnum],drift_model=pipeline.get_drift_model(), drift_order=pipeline.get_drift_order(), high_pass=pipeline.get_drift_hp(), hrf_model='SPM', stim_dur=data[sessnum].annotations.duration[0]))   
        
        
        generalizedDMDF = generalized_design_matrix(design_matrix, boolSSD, short_channels, sessionNumber, session)
        
        
        paddedBPfilt = padded_SNIRF(data, sessionNumber)
        
        
        glm_est = mne_nirs.statistics.run_glm(paddedBPfilt, generalizedDMDF, noise_model=pipeline.get_glm_noise_model()) # Run GLM
        
        
        glm_DF=glm_est.to_dataframe()
        
        
        rejected, corr_p_val=mne.stats.fdr_correction(glm_DF.p_value)
        glm_DF["fdr_p_value"]=corr_p_val
        
    elif (pipeline.get_glm_model()=='not-gen-separate'):
        glm_DF=pd.DataFrame(columns=['Condition', 'df', 'mse', 'p_value', 'se', 't', 'theta', 'Source', 'Detector', 'Chroma', 'Significant', 'ch_name', 'fdr_p_value'])
        for sessnum in range(sessionNumber):
            if(boolSSD): 
                short_channels.append(mne_nirs.channels.get_short_channels(data[sessnum].copy()))
            else : short_channels=[]
            current_glm_DF = pd.DataFrame()
            long_channels.append(mne_nirs.channels.get_long_channels(data[sessnum].copy()))
            design_matrix=mne_nirs.experimental_design.make_first_level_design_matrix(long_channels[sessnum],drift_model=pipeline.get_drift_model(), drift_order=pipeline.get_drift_order(), high_pass=pipeline.get_drift_hp(), hrf_model='SPM', stim_dur=data[sessnum].annotations.duration[0])
            
        
            current_glm_est=mne_nirs.statistics.run_glm(long_channels[sessnum], design_matrix, noise_model=pipeline.get_glm_noise_model()) # Run GLM
        
        
            current_glm_DF=current_glm_est.to_dataframe()
        
        
            rejected, corr_p_val=mne.stats.fdr_correction(current_glm_DF.p_value)
            current_glm_DF["fdr_p_value"]=corr_p_val
            current_glm_DF["Condition"]=current_glm_DF["Condition"].add("-"+str(sessnum+1))
            glm_DF=glm_DF.merge(current_glm_DF, how='outer')
        
        
    return glm_DF


def individual_analysis(subject_path, pipeline, ID):
    """
    For each session, it gets Raw SNIRF convert it to optical density then correct it with temporal derivative distribution repair. The quality control 
    is calculated with SCI and PSP and channels that don't pass bad channels both tests are saved in the list badch. Optical density corrected with tddr 
    is converted to concentration changes, then the signal is filtered if desired. The GLM is run with the result of generalized_design_matrix() and padded_SNIRF().

    Parameters
    ----------
    subject_path : BIDSPath
        BIDSPath class object from mne_bids.
    pipeline : Pipeline
        Object from Pipeline class.
    ID : String
        Subject ID.
    Returns
    -------
    glmDF : dataframe
        Dataframe of generalized GLM result.
    badch : List(String)
        List of channels name that don't pass the quality test.

    """
    raw_data = list()
    OD = list()
    ODMACorr = list()
    Conc = list()
    BPfilt = list()
    badch = list()
    
    
    #Data retrieval, conversion, quality check, filtering of every sessions
    
    
    for i in range(len(pipeline.get_sessions())): #process each sessions of input subject with following pipeline : Raw -> OD -> OD tddr -> SCI & PSP -> Conc -> Filter
        if (len(pipeline.get_sessions()) != 1): bids_path=subject_path.update(session = pipeline.get_sessions()[i])  #Update of bids_path, if there is only one session this line does not work
        raw_data.insert(i, mne_bids.read_raw_bids(bids_path = subject_path, extra_params = None, verbose = False))#list of loaded raw data of every sessions of a subject
        if pipeline.get_crop()!=(None,None): raw_data[i].crop(raw_data[i].annotations.onset[0]-pipeline.get_crop()[0],raw_data[i].annotations.onset[-1]+pipeline.get_crop()[1])
        OD.insert(i,mne.preprocessing.nirs.optical_density(raw_data[i])) #list of Optical Density (converted from Raw data) of every sessions of a subject
        ODMACorr.insert(i,mne.preprocessing.nirs.temporal_derivative_distribution_repair(OD[i])) #list of Optical density corrected for motion actifact with tddr (from OD) of every sessions of a subject
        
        
        #Data conversion from Optical density to Concentration changes
        Conc.insert(i,mne.preprocessing.nirs.beer_lambert_law(ODMACorr[i],ppf = 6))
        #Bandpass filtering if option is chosen
        if pipeline.filter_param != [None,None] : BPfilt.insert(i,Conc[i].copy().filter(pipeline.get_filter_param()[0], pipeline.get_filter_param()[1], l_trans_bandwidth = 0.01, h_trans_bandwidth = 0.01, verbose = False))
        else: BPfilt = Conc


        #quality check
        bad_channels = quality_check(OD[i], raw_data[i], BPfilt[i], pipeline.get_quality_check_option(),pipeline)
        badch.insert(i,bad_channels)
    
    
    #short channel detection
    boolSSD = mne.preprocessing.nirs.nirs.short_channels(BPfilt[0].info).any() 
    
    glm_DF=run_glm(pipeline.get_sessions(), len(pipeline.get_sessions()), boolSSD, BPfilt, pipeline)

    # Convert regresison result to dataframe for better storage
    glm_DF["ID"] = ID


    return glm_DF, badch

#_______________________________________________________________________________________________________________________________________________________________________________

def main():

    
    pp=Pipeline(bids_path, quality_check_option, crop, trigger_length, filter_param, drift_model, drift_hp, drift_order, glm_model, glm_noise_model, excel)


    #create derivative and Fresh-pipeline directory if they do not exist
    if(not(path.isdir(pp.get_bids_path()+"/derivatives/FRESH-pipeline"))):
        makedirs(pp.get_bids_path()+"/derivatives/FRESH-pipeline")
    
    
    #BIDS format data retreival
    dataset = mne_bids.BIDSPath(root=pp.get_bids_path(), task=mne_bids.get_entity_vals(pp.get_bids_path(), 'task')[0], datatype="nirs", suffix="nirs", extension=".snirf")
    subjects = mne_bids.get_entity_vals(pp.get_bids_path(), 'subject') # gets list of subjects ID
    session = mne_bids.get_entity_vals(pp.get_bids_path(),'session') # gets list of sessions names
    if (session==[]): pp.set_sessions([None])
    else : pp.set_sessions(session)
    
    
    #delete existing files
    for sub in subjects:
        if path.exists(pp.get_bids_path()+"/derivatives/FRESH-pipeline/sub_"+sub+"_glmDF.pkl"):
            remove(pp.get_bids_path()+"/derivatives/FRESH-pipeline/sub_"+sub+"_glmDF.pkl")
        if path.exists(pp.get_bids_path()+'/derivatives/FRESH-pipeline/sub_'+sub+'_bad_channels.pkl'):
            remove(pp.get_bids_path()+'/derivatives/FRESH-pipeline/sub_'+sub+'_bad_channels.pkl')
        if path.exists(pp.get_bids_path()+'/derivatives/FRESH-pipeline/Figure-sub_'+sub+'.png'):
            remove(pp.get_bids_path()+'/derivatives/FRESH-pipeline/Figure-sub_'+sub+'.png')
    
    #Save pipeline parameters
    parametersFile = open(pp.get_bids_path()+"/derivatives/FRESH-pipeline/parmetersFile.txt","w")
    parametersFile.write(pp.to_string())
    
    #processing and saving of each subjects
    for sub in subjects:
        sub_bids_path=dataset.update(subject=sub)   # change data path to go throught each subjects data
        print('\n Subject ' + sub +'\n __________________________')
        glmDF, badch = individual_analysis(sub_bids_path,pp,sub)  # individual analysis for each subjects
        parametersFile.write("\n\nSubject "+sub+"\n________________\nBad channels with "+str(quality_check_option)+" quality check option :\n"+str(badch[0][0:len(badch[0]):2]))
        glmDF.to_pickle(pp.get_bids_path()+"/derivatives/FRESH-pipeline/sub_"+sub+"_glmDF.pkl")   # save GLM results in dataframe 
        if (pp.get_excel()): glmDF.to_excel(pp.get_bids_path()+"/derivatives/FRESH-pipeline/sub_"+sub+"_glmDF.xlsx")                
        with open(pp.get_bids_path()+'/derivatives/FRESH-pipeline/sub_'+sub+'_bad_channels.pkl', 'wb') as f:  # save bad channel for data exploration script
            dump(badch, f)
            
    parametersFile.close()

    
    #Save pipeline data structure
    with open(pp.get_bids_path()+'/derivatives/FRESH-pipeline/pipeline.pkl', 'wb') as f:  # save bad channel for data exploration script
            dump(pp, f)


if __name__ == "__main__":
    
    
    #options for command line running
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description=textwrap.dedent('''Process the input dataset in BIDS format. Process results will be stores in BidsPath/derivatives/FRESH-pipeline.
        - To see further details on filter : https://mne.tools/stable/generated/mne.io.Raw.html#mne.io.Raw.filter
        - To see further details on design matrix : https://mne.tools/mne-nirs/stable/generated/mne_nirs.experimental_design.make_first_level_design_matrix.html#mne-nirs-experimental-design-make-first-level-design-matrix
        - To see further details on glm : https://mne.tools/mne-nirs/stable/generated/mne_nirs.statistics.run_glm.html#mne_nirs.statistics.run_glm 
        - To analyse results of said processing use FRESHpipeline-GroupProcessing.py'''))
    parser.add_argument('bids_path', type = str, help = 'Path to DataFolder in bids format - for Windows \ must be changes to /')
    parser.add_argument('--crop' '-c', default = (None,None), help = 'Crop data')
    parser.add_argument('--quality_check_option', type = str, default = 'sci-psp', help = 'Type of quality check. "sci-psp" means Scalp Coupling Index (SCI) > 0.8 or Peak Spectral Power (PSP) < 0.1. "snr" means Signal to Noise Ratio > 8. None means no quality check will ne applied. "correlate" means that hbo and hbr signals must be uncorrelated ')
    parser.add_argument('--trigger_length', type = float, default = 'None', help = 'If quality check option "correlate" is chosen this option is the length in seconds of block average use to calcul correlation of channels that the correlation is calculated on. None, option will use the default trigger length.')
    parser.add_argument('--filter_param','-f', default = (0.01,0.09), help = 'Filter parameters [Low frequency, High frequency], default is [0.01,0.09], no filter is [None,None], lowpass filter is [None, High Frequency], highpass filter is [Low Frequency, None]. Width of transition bands are 0.01 each.' )
    parser.add_argument('--drift_model','-d', type = str, default = None, help = 'Specifies the desired drift model, can be : ‘cosine’, ‘polynomial’, None.')
    parser.add_argument('--drift_hp', type = float, default = 0.01, help = 'High-pass frequency in case the drift mode is cosine (in Hz). Default=0.01.')
    parser.add_argument('--drift_order', type = int, default = 1, help = 'Order of the drift model (in case it is polynomial). Default=1.')
    parser.add_argument('--glm_model', type = str, default = 'gen-separate', help = "'gen-separate', 'gen-same','not-gen-separate'and are the options, 'gen-separate' creates two generalized matrix (where the design matrix of each session is combined in one) one for hbo and one for hbr with corresponding mean of short channels.  'gen-same' creates one generalized matrix used for hbo and hbr with the mean of short channels for hbo and hbr as 2 additional regressors. 'not-gen-separate' create one design matrix without short channel regression for each session and both chromophores.")
    parser.add_argument('--glm_noise_model', type = str, default = 'arN', help = 'The temporal variance model.Values can be ‘ar1’, ‘ols’, ‘arN’, ‘auto’. Default ‘arN’ will replane N by recommended value : 4 times sampling frequency. The AR model can be set to any integer value by modifying the value of N.')
    parser.add_argument('--excel', type = bool, default = 'False', help = 'Save dataframe into excel files for better readability')                                                                                                                                                                                                                                                                                                               
    args = parser.parse_args()
    bids_path = args.bids_path  
    quality_check_option = args.quality_check_option   
    crop=args.crop   
    trigger_length = args.trigger_length
    filter_param = args.filter_param          
    drift_model = args.drift_model
    drift_hp = args.drift_hp
    drift_order = args.drift_order
    glm_model = args.glm_model
    glm_noise_model = args.glm_noise_model
    excel = args.excel

   
    main() 