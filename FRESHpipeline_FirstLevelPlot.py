# -*- coding: utf-8 -*-
"""
Created on Mon Jan 30 11:17:51 2023

# Authors: Nolwenn Jegou <nolwenn.jegou@inria.fr>
"""
import pandas as pd
import mne
import mne_bids
import matplotlib.pyplot as plt
import numpy as np
import matplotlib as mpl
from pickle import load, dump
import argparse
import textwrap



def plot_1_dimention(cleandata, axes, legend, pipeline, coordinates, bad_channels, subject_number):
    """
    Plot and save a one dimentionnal image of the GLM results when there isn't multiple sessions and multiple tasks.

    Parameters
    ----------
    cleandata : Dataframe
        Dataframe with organized data.
    axes : Array of Matplotlib Axes
        Axe along with every GLM result wil be plot.
    legend : Array of matplotlib.pyplot.lines.Line2D
        Legend elements.
    pipeline : Pipeline
        Object from Pipeline class.
    coordinates : TYPE
        DESCRIPTION.
    bad_channels : List of String
        List of channels rejected by quality check.
    subject_number : Int
        subject number.

    Returns
    -------
    None.

    """

    
    if len(pipeline.get_sessions())==1 and len(pipeline.get_task_names())==1: axes=[axes] #plot definition for data without session
   
    i=0
    for ax in axes :
        if (len(pipeline.get_sessions())==1): ax.set_ylabel("Task "+pipeline.get_task_names()[i],fontsize='x-large')
        else: ax.set_title("Session "+pipeline.get_sessions()[i],fontsize='x-large')
        ax.scatter(0,0, s=160000, facecolors='none', edgecolors='k')
        ax.scatter(0,0.125, s=1000, marker='^', facecolors='none', edgecolors='k')
        ax.set_xlim([-0.10,0.10])
        ax.set_ylim([-0.10,0.14])
        ax.set_frame_on(False)
        ax.set_xticks([])
        ax.set_yticks([])
        ax.legend(handles=legend, loc="lower center", bbox_to_anchor=(0.5,-0.13))
        i=i+1
        
        
    fignum=0
    for sessnum, sess in enumerate(pipeline.get_sessions()):
        bad_channels_clean=[item[:5] for item in bad_channels[subject_number][sessnum]]
        
        
        for tasknum, taskn in enumerate(pipeline.get_task_names()):

            
            for chname in set(cleandata.hbo_ch_name): 
                
                
                chcoord=coordinates._get_ch_pos()[chname]
                
                if len(pipeline.get_sessions())==1: 
                    chnlline=cleandata[(cleandata.hbo_ch_name==chname) & (cleandata.condition.str.contains(taskn))] #get line from cleandata dataframe  corresponding to current task and channel name
                else:  
                    chnlline=cleandata[(cleandata.hbo_ch_name==chname) & (cleandata.condition.str.contains(sess)) & (cleandata.condition.str.contains(taskn))] #get line from cleandata dataframe corresponding to current task, session and channel name
               
                if (np.isin(chname[0:-4],bad_channels_clean)) :
                    axes[fignum].scatter(chcoord[0],chcoord[1], s=100, facecolors='none', edgecolors='k')
                elif (bool(chnlline.activated.values)):
                    axes[fignum].scatter(chcoord[0],chcoord[1], s=200, color='r')
                else:
                    axes[fignum].scatter(chcoord[0],chcoord[1], s=100, color='k',)
                  
                    
            fignum=fignum+1
            
            
def plot_1D_hbo_and_hbr(cleandata, axes, legend, pipeline, coordinates, bad_channels, subject_number):
    """
    Plot and save a one dimentionnal image of the GLM results when there isn't multiple sessions and multiple tasks.

    Parameters
    ----------
    cleandata : Dataframe
        Dataframe with organized data.
    axes : Array of Matplotlib Axes
        Axe along with every GLM result wil be plot.
    legend : Array of matplotlib.pyplot.lines.Line2D
        Legend elements.
    pipeline : Pipeline
        Object from Pipeline class.
    coordinates : TYPE
        DESCRIPTION.
    bad_channels : List of String
        List of channels rejected by quality check.
    subject_number : Int
        subject number.

    Returns
    -------
    None.

    """
    
    
    limhbo=[min(cleandata.beta_hbo),max(cleandata.beta_hbo)]
    limhbr=[min(cleandata.beta_hbr),max(cleandata.beta_hbr)]
    red = plt.cm.YlOrRd(np.linspace(0,1,num=11))
    blue = plt.cm.GnBu_r(np.linspace(0,1,num=11))
    
    if len(pipeline.get_sessions())==1 and len(pipeline.get_task_names())==1: axes=[axes] #plot definition for data without session
   

    for i in range(int(len(axes)/2)):
        if (len(pipeline.get_sessions())==1):  axes[2*i].set_ylabel("Task "+pipeline.get_task_names()[i],fontsize='x-large')
        else:  axes[2*i].set_title("Session "+pipeline.get_sessions()[i],fontsize='x-large')
        
        axes[2*i].scatter(0,0, s=160000, facecolors='none', edgecolors='k')
        axes[2*i].scatter(0,0.125, s=1000, marker='^', facecolors='none', edgecolors='k')
        axes[2*i].set_xlim([-0.10,0.10])
        axes[2*i].set_ylim([-0.10,0.14])
        axes[2*i].set_frame_on(False)
        axes[2*i].set_xticks([])
        axes[2*i].set_yticks([])
        axes[2*i].legend(handles=legend[0:2], loc="lower center", bbox_to_anchor=(0.5,-0.11))
        plt.colorbar(mpl.cm.ScalarMappable(norm= mpl.colors.Normalize(vmax=limhbo[1],vmin=limhbo[0]), cmap=mpl.cm.YlOrRd), ax=axes[2*i], anchor=(0.0,0.5))

        axes[2*i+1].scatter(0,0, s=160000, facecolors='none', edgecolors='k')
        axes[2*i+1].scatter(0,0.125, s=1000, marker='^', facecolors='none', edgecolors='k')
        axes[2*i+1].set_xlim([-0.10,0.10])
        axes[2*i+1].set_ylim([-0.10,0.14])
        axes[2*i+1].set_frame_on(False)
        axes[2*i+1].set_xticks([])
        axes[2*i+1].set_yticks([])
        axes[2*i+1].legend(handles=legend[0:2], loc="lower center", bbox_to_anchor=(0.5,-0.11))
        plt.colorbar(mpl.cm.ScalarMappable(norm= mpl.colors.Normalize(vmax=limhbr[1],vmin=limhbr[0]), cmap=mpl.cm.GnBu_r), ax=axes[2*i+1], anchor=(0.0,0.5))
        
        
    fignum=0
    for sessnum, sess in enumerate(pipeline.get_sessions()):
        #bad_channels_clean=[item[:5] for item in bad_channels[subject_number][sessnum]]
        
        
        for tasknum, taskn in enumerate(pipeline.get_task_names()):

            
            for chname in set(cleandata.hbo_ch_name): 
                
                
                chcoord=coordinates._get_ch_pos()[chname]
                
                if len(pipeline.get_sessions())==1: 
                    chnlline=cleandata[(cleandata.hbo_ch_name==chname) & (cleandata.condition.str.contains(taskn))] #get line from cleandata dataframe  corresponding to current task and channel name
                else:  
                    chnlline=cleandata[(cleandata.hbo_ch_name==chname) & (cleandata.condition.str.contains(sess)) & (cleandata.condition.str.contains(taskn))] #get line from cleandata dataframe corresponding to current task, session and channel name
               
                # if (np.isin(chname[0:-4],bad_channels_clean)) :
                #     axes[fignum].scatter(chcoord[0],chcoord[1], s=100, facecolors='none', edgecolors='k')
                if (chnlline.corr_p_value_hbo.values[0]<0.05) : 
                    axes[fignum].scatter(chcoord[0],chcoord[1], s=200, color=red[int(10*(chnlline.beta_hbo-limhbo[0])/(limhbo[1]-limhbo[0]))])
                else:
                    axes[fignum].scatter(chcoord[0],chcoord[1], s=200, marker="X", color=red[int(10*(chnlline.beta_hbo-limhbo[0])/(limhbo[1]-limhbo[0]))])

                if (chnlline.corr_p_value_hbr.values[0]<0.05) : 
                    axes[fignum+1].scatter(chcoord[0],chcoord[1], s=200, color=blue[int(10*(chnlline.beta_hbr-limhbr[0])/(limhbr[1]-limhbr[0]))])
                else:
                    axes[fignum+1].scatter(chcoord[0],chcoord[1], s=200, marker="X", color=blue[int(10*(chnlline.beta_hbr-limhbr[0])/(limhbr[1]-limhbr[0]))])

                  
                    
            fignum=fignum+2


#plot images for 2 dimentionnal dataset
def plot_2_dimention(cleandata, axes, legend, pipeline, coordinates, bad_channels, subject_number):
    """
    Plot and save a two dimentionnal image of the GLM results when there is multiple sessions and multiple tasks.

    Parameters
    ----------
    cleandata : dataframe
        Dataframe with organized data.
    axes : Array of Matplotlib Axes
        Axe along with every GLM result wil be plot.
    legend : Array of matplotlib.pyplot.lines.Line2D
        Legend elements.
    pipeline : Pipeline
        Object from Pipeline class.
    coordinates : DigMontage
        Montage for digitized electrode and headshape position data.
    bad_channels : List of String
        List of channels rejected by quality check.
    subject_number : Int
        subject number.

    Returns
    -------
    None.

    """
    
    
    for ax in axes :
        for sessnum in range(len(pipeline.get_sessions())):
            axes[0,sessnum].set_title("Session "+pipeline.get_sessions()[sessnum],fontsize='x-large')
            for tasknum in range(len(pipeline.get_task_names())):
                if sessnum==0 : axes[tasknum,sessnum].set_ylabel("Task "+pipeline.get_task_names()[tasknum],fontsize='x-large')
                axes[tasknum,sessnum].scatter(0,0, s=160000, facecolors='none', edgecolors='k')
                axes[tasknum,sessnum].scatter(0,0.125, s=1000, marker='^', facecolors='none', edgecolors='k')
                axes[tasknum,sessnum].set_xlim([-0.10,0.10])
                axes[tasknum,sessnum].set_ylim([-0.10,0.14])
                axes[tasknum,sessnum].set_frame_on(False)
                axes[tasknum,sessnum].set_xticks([])
                axes[tasknum,sessnum].set_yticks([])
                axes[tasknum,sessnum].legend(handles=legend, loc="lower center", bbox_to_anchor=(0.5,-0.13))
                
                
    for sessnum,sess in enumerate(pipeline.get_sessions()):
        bad_channels_clean=[item[:5] for item in bad_channels[subject_number][sessnum]]
        
        
        for tasknum,taskn in enumerate(pipeline.get_task_names()):
            for chname in set(cleandata.hbo_ch_name): 
                
                
                chcoord=coordinates._get_ch_pos()[chname]
                chnlline=cleandata[(cleandata.hbo_ch_name==chname) & (cleandata.condition.str.contains(sess)) & (cleandata.condition.str.contains(taskn))] #get line from cleandata dataframe  corresponding to current task, session and channel name
                
                if (np.isin(chname[0:-4],bad_channels_clean)) : 
                    axes[tasknum,sessnum].scatter(chcoord[0],chcoord[1], s=100, facecolors='none', edgecolors='k')
                
                elif (bool(chnlline.activated.values)):
                    axes[tasknum,sessnum].scatter(chcoord[0],chcoord[1], s=200, color='r')
                
                else:
                    axes[tasknum,sessnum].scatter(chcoord[0],chcoord[1], s=100, color='k')


def plot_2D_hbo_and_hbr(cleandata, axes, legend, pipeline, coordinates, bad_channels, subject_number):
    """
    Plot and save a two dimentionnal image of the GLM results when there is multiple sessions and multiple tasks.

    Parameters
    ----------
    cleandata : dataframe
        Dataframe with organized data.
    axes : Array of Matplotlib Axes
        Axe along with every GLM result wil be plot.
    legend : Array of matplotlib.pyplot.lines.Line2D
        Legend elements.
    pipeline : Pipeline
        Object from Pipeline class.
    coordinates : DigMontage
        Montage for digitized electrode and headshape position data.
    bad_channels : List of String
        List of channels rejected by quality check.
    subject_number : Int
        subject number.

    Returns
    -------
    None.

    """
    
    
    limhbo=[min(cleandata.beta_hbo),max(cleandata.beta_hbo)]
    limhbr=[min(cleandata.beta_hbr),max(cleandata.beta_hbr)]
    red = plt.cm.YlOrRd(np.linspace(0,1,num=11))
    blue = plt.cm.GnBu_r(np.linspace(0,1,num=11))
    
    
    for axnum, ax in enumerate(axes) :
        for sessnum in range(len(pipeline.get_sessions())):
            if sessnum!=0 : 
                axes[0,sessnum*2].set_title("Session "+pipeline.get_sessions()[sessnum],fontsize='x-large')
                axes[0,sessnum*2+1].set_title("Session "+pipeline.get_sessions()[sessnum],fontsize='x-large')
            
            if sessnum==0 : axes[axnum,sessnum].set_ylabel("Task "+pipeline.get_task_names()[axnum],fontsize='x-large')
            axes[axnum,sessnum*2].scatter(0,0, s=160000, facecolors='none', edgecolors='k')
            axes[axnum,sessnum*2].scatter(0,0.125, s=1000, marker='^', facecolors='none', edgecolors='k')
            axes[axnum,sessnum*2].set_xlim([-0.10,0.10])
            axes[axnum,sessnum*2].set_ylim([-0.10,0.14])
            axes[axnum,sessnum*2].set_frame_on(False)
            axes[axnum,sessnum*2].set_xticks([])
            axes[axnum,sessnum*2].set_yticks([])
            axes[axnum,sessnum*2].legend(handles=legend[0:2], loc="lower center", bbox_to_anchor=(0.5,-0.11))
            plt.colorbar(mpl.cm.ScalarMappable(norm= mpl.colors.Normalize(vmax=limhbo[1],vmin=limhbo[0]), cmap=mpl.cm.YlOrRd), ax=axes[axnum,sessnum*2], anchor=(0.0,0.5))
            
            
            axes[axnum,sessnum*2+1].scatter(0,0, s=160000, facecolors='none', edgecolors='k')
            axes[axnum,sessnum*2+1].scatter(0,0.125, s=1000, marker='^', facecolors='none', edgecolors='k')
            axes[axnum,sessnum*2+1].set_xlim([-0.10,0.10])
            axes[axnum,sessnum*2+1].set_ylim([-0.10,0.14])
            axes[axnum,sessnum*2+1].set_frame_on(False)
            axes[axnum,sessnum*2+1].set_xticks([])
            axes[axnum,sessnum*2+1].set_yticks([])
            axes[axnum,sessnum*2+1].legend(handles=legend[0:2], loc="lower center", bbox_to_anchor=(0.5,-0.10))
            plt.colorbar(mpl.cm.ScalarMappable(norm= mpl.colors.Normalize(vmax=limhbr[1],vmin=limhbr[0]), cmap=mpl.cm.GnBu_r), ax=axes[axnum,sessnum*2+1], anchor=(0.0,0.5))
       
    
    for sessnum,sess in enumerate(pipeline.get_sessions()):
        #get bad channels name from current subject and session number
        #bad_channels_clean=[item[:5] for item in bad_channels[subject_number][sessnum]]  #makes output too complicated to read
        
        
        for tasknum,taskn in enumerate(pipeline.get_task_names()):
            for chname in set(cleandata.hbo_ch_name): 
                
                
                chcoord=coordinates._get_ch_pos()[chname]
                
                
                if (pipeline.get_sessions()!=[None]): chnlline=cleandata[(cleandata.hbo_ch_name==chname) & (cleandata.condition.str.contains(sess)) & (cleandata.condition.str.contains(taskn))] #get line from cleandata dataframe  corresponding to current task, session and channel name
                else : chnlline=cleandata[(cleandata.hbo_ch_name==chname) & cleandata.condition.str.contains(taskn)] #get line from cleandata dataframe  corresponding to current task, session and channel name


                # if (np.isin(chname[0:-4],bad_channels_clean)) :
                #     axes[tasknum,sessnum*2].scatter(chcoord[0],chcoord[1], s=110, facecolors='none', edgecolors='k')
                #     axes[tasknum,sessnum*2+1].scatter(chcoord[0],chcoord[1], s=110, facecolors='none', edgecolors='k')
                    
                if (chnlline.corr_p_value_hbo.values[0]<0.05) : 
                    axes[tasknum,sessnum*2].scatter(chcoord[0],chcoord[1], s=200, color=red[int(10*(chnlline.beta_hbo-limhbo[0])/(limhbo[1]-limhbo[0]))])
                else:
                    axes[tasknum,sessnum*2].scatter(chcoord[0],chcoord[1], s=200, marker="X", color=red[int(10*(chnlline.beta_hbo-limhbo[0])/(limhbo[1]-limhbo[0]))])

                if (chnlline.corr_p_value_hbr.values[0]<0.05) : 
                    axes[tasknum,sessnum*2+1].scatter(chcoord[0],chcoord[1], s=200, color=blue[int(10*(chnlline.beta_hbr-limhbr[0])/(limhbr[1]-limhbr[0]))])
                else:
                    axes[tasknum,sessnum*2+1].scatter(chcoord[0],chcoord[1], s=200, marker="X", color=blue[int(10*(chnlline.beta_hbr-limhbr[0])/(limhbr[1]-limhbr[0]))])
                    

def create_clean_dataframe(DF, taskname, sessions):
    """
    Creates a dataframe that summarize only useful informations from GLM rsults dataframe : conditions, hbo channel name, hbo beta value, hbr beta value, corrected hbo p values (FDR), 
    coorected hbr p values(FDR), and the conclusion if the channel is activated or not (corrected (hbo and hbr) p_value True and (for hbo beta positive or for hbr beta negative))

    Parameters
    ----------
    DF : Dataframe
        Dataframe of GLM result
    taskname : list[String]
        List of task names in a dataset.
    sessions : List[String]
        List of session names in a dataset.

    Returns
    -------
    cleandata : Dataframe
        dataframe that summarize only useful informations.

    """
    
    
    cleandata = pd.DataFrame(columns=['condition', 'hbo_ch_name', 'beta_hbo', 'beta_hbr' ,'corr_p_value_hbo', 'corr_p_value_hbr', 'activated'])
    i=0
    #data organization if their is different sessions
    for cond in taskname:
        for sess in sessions:
            if (len(sessions) == 1):
                condhbr=DF[(DF["Chroma"]=='hbr') & (DF["Condition"].str.contains(cond))]  # get dataframe of tasks and chroma hbr
                condhbo=DF[(DF["Chroma"]=='hbo') & (DF["Condition"].str.contains(cond))]  # get dataframe of tasks and chroma hbo
           
            else :
                condhbr=DF[(DF["Chroma"]=='hbr') & (DF["Condition"].str.contains(cond)) & (DF["Condition"].str.contains(sess))]  # get dataframe of tasks and chroma hbr
                condhbo=DF[(DF["Chroma"]=='hbo') & (DF["Condition"].str.contains(cond)) & (DF["Condition"].str.contains(sess))]  # get dataframe of tasks and chroma hbo
           
            
            for j in range(0,len(condhbr)):
                cleandata.loc[i]=[condhbo.iloc[j].Condition,condhbo.iloc[j].ch_name,condhbo.iloc[j].theta,condhbr.iloc[j].theta,condhbo.iloc[j].fdr_p_value,condhbr.iloc[j].fdr_p_value,(condhbo.iloc[j].fdr_p_value<0.05 and condhbr.iloc[j].fdr_p_value<0.05 and condhbo.iloc[j].theta>0 and condhbr.iloc[j].theta<0)]
                i=i+1
                
                
    return cleandata


def main():
    
        
    #get BIDS dataset to get number of sessions
    dataset=mne_bids.BIDSPath(root=pipeline.get_bids_path(), task=mne_bids.get_entity_vals(pipeline.get_bids_path(), 'task')[0], datatype="nirs", suffix="nirs", extension=".snirf")
    subjects = mne_bids.get_entity_vals(pipeline.get_bids_path(), 'subject')
    
    
    #import the GLM dataframe and bad_channels list created by FRESHpipeline-GroupProcessing.py in lists.
    general_glm_est_DF=list()
    general_bad_channels=list()
    for idx, sub in enumerate(subjects):
        general_glm_est_DF.append(pd.read_pickle(pipeline.get_bids_path()+"/derivatives/FRESH-pipeline/sub_"+sub+"_glmDF.pkl"))
        with open(pipeline.get_bids_path()+'/derivatives/FRESH-pipeline/sub_'+sub+'_bad_channels.pkl', 'rb') as f:
            general_bad_channels.append(load(f))
    
    
    #import channel coordinates
    dataset.update(subject=subjects[0], session=pipeline.get_sessions()[0])
    raw_data = mne_bids.read_raw_bids(bids_path=dataset, extra_params=None, verbose=True)  # get raw data to explore coordinates
    pipeline.set_task_names(np.unique(raw_data.annotations.description)) # get task values
    coordinates = mne.preprocessing.nirs.beer_lambert_law(mne.preprocessing.nirs.optical_density(raw_data)).get_montage()
    
       
    with open(pipeline.get_bids_path()+'/derivatives/FRESH-pipeline/pipeline.pkl', 'wb') as f:  # save bad channel for data exploration script
            dump(pipeline, f)
    
    #plot session result per subject
    for subjectlvli, DF in enumerate(general_glm_est_DF): 
    
        
        #prepare a plot per subject for each sessions  
        if (plot=='hbo&hbr'): 
            fig, axes = plt.subplots(nrows=len(pipeline.get_task_names()), ncols=len(pipeline.get_sessions())*2, figsize=(len(pipeline.get_sessions())*8*2,len(pipeline.get_task_names())*7))#,gridspec_kw=dict(width_ratios=[4, 1]))
        else : 
            fig, axes = plt.subplots(nrows=len(pipeline.get_task_names()), ncols=len(pipeline.get_sessions()), figsize=(len(pipeline.get_sessions())*7,len(pipeline.get_task_names())*7))#,gridspec_kw=dict(width_ratios=[4, 1]))
       
        fig.suptitle("Subject "+subjects[subjectlvli],fontsize='xx-large',weight='bold')
        plt.tight_layout(pad=3)
        print('\n Subject ' + subjects[subjectlvli] +'\n __________________________')
        
        
        #gets rid of condition unrelated to tasks in the dataframe
        DF=pd.concat(DF[DF["Condition"].str.contains(taskn)] for taskn in pipeline.get_task_names()) #get hbo line from model_df corresponding to current condition and channel name
        
        
        summarizedDF=create_clean_dataframe(DF, pipeline.get_task_names(), pipeline.get_sessions())
        
        
        #plot functions defined above
        if(plot=='hbo&hbr'):
            redot=mpl.lines.Line2D([], [], marker='o', color='gray', linestyle='None', markersize=10, label='Activated channel', alpha=1)
            blackdot=mpl.lines.Line2D([], [], marker='X', color='gray', linestyle='None', markersize=8, label='Not activated channel', alpha=1)
            circle=mpl.lines.Line2D([], [], marker='o', linestyle='None', markersize=8, markerfacecolor='none', markeredgecolor='k', label='Did not pass quality test', alpha=1)
            if (len(pipeline.get_sessions())!=1 and len(pipeline.get_sessions())):plot_1D_hbo_and_hbr( summarizedDF, axes, [redot,blackdot,circle], pipeline, coordinates, general_bad_channels, subjectlvli)
            else: plot_2D_hbo_and_hbr( summarizedDF, axes, [redot,blackdot,circle], pipeline, coordinates, general_bad_channels, subjectlvli)
        else :
            redot=mpl.lines.Line2D([], [], marker='o', color='red', linestyle='None', markersize=10, label='Activated channel', alpha=1)
            blackdot=mpl.lines.Line2D([], [], marker='o', color='black', linestyle='None', markersize=8, label='Not activated channel', alpha=1)
            circle=mpl.lines.Line2D([], [], marker='o', linestyle='None', markersize=8, markerfacecolor='none', markeredgecolor='k', label='Did not pass quality test', alpha=1)
            if(len(pipeline.get_task_names())==1 or len(pipeline.get_sessions())==1): plot_1_dimention( summarizedDF, axes, [redot,blackdot,circle], pipeline, coordinates, general_bad_channels, subjectlvli)
            else: plot_2_dimention( summarizedDF, axes, [redot,blackdot,circle], pipeline, coordinates, general_bad_channels, subjectlvli)
    
        if display==True : plt.show()
    
            
        fig.savefig(pipeline.get_bids_path()+'/derivatives/FRESH-pipeline/Figure-sub_'+subjects[subjectlvli]+'-'+plot+'.png')
        
        


if __name__ == "__main__":

    
    #option for command line running
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description=textwrap.dedent('''First level analysis of a dataset processing. Results will be stores in BidsPath/derivatives/FRESH-pipeline. 
    Must be run after FRESHpipeline-GroupProcessing.py'''))
    parser.add_argument('bids_path', type=str, help='Path to DataFolder in bids format - for Windows "\" must be changes to "/"')
    parser.add_argument('--display','-d', type=bool, default=False, help='Display figure for each subject during processing.')
    parser.add_argument('--plot', type=str, default='conjonction', help="'conjonction' display results taking into account hbo and hbr parameters. 'hbo&hbr' display hbo and hbr results in different plots.")
    args = parser.parse_args()
    display = args.display
    bids_path = args.bids_path
    plot=args.plot
    

    from FRESHpipeline_FirstLevelProcessing import Pipeline  
    with open(bids_path+'/derivatives/FRESH-pipeline/pipeline.pkl', 'rb') as file:
        pipeline = load(file)
        
        
    main() 

    