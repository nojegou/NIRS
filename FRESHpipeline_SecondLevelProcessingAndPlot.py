# -*- coding: utf-8 -*-
"""
Created on Tue Apr 25 16:25:58 2023

# Authors: Nolwenn Jegou <nolwenn.jegou@inria.fr>
"""
import mne 
import mne_nirs
import pandas as pd
import mne_bids
import argparse
from textwrap import dedent
import statsmodels.formula.api as smf
from pickle import load
import datetime
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
import numpy as np

def plot_2nd_level_res(fig, axes, model_df, coordinates, genset_bad_channels):
    """
    plot and save the second level GLM of data. Each point is the center of each channel. A channel as marked as un

    Parameters
    ----------
    fig : matplotlib.figure.Figure
        DESCRIPTION.
    axes : Array of MatplotlTYPE Axes
        Axe along with every GLM result wil be plot.
    model_df : Dataframe
        Result of second level analysis.
    coordinates : mne.channels.montage.DigMontage
        Coordinates of channels.
    genset_bad_channels : Set
        Set of bad channels included in all subjects.

    Returns
    -------
    None.

    """

    fig.suptitle("Second level GLM",fontsize='xx-large',weight='bold')
    plt.tight_layout(pad=2)
    
    #legend definition
    redot=mpl.lines.Line2D([], [], color='r', marker='o', linestyle='None', markersize=10, label='Activated channel', alpha=1)
    blackdot=mpl.lines.Line2D([], [], color='k', marker='o', linestyle='None', markersize=8, label='Not activated channel', alpha=1)
    circle=mpl.lines.Line2D([], [], marker='o', linestyle='None', markersize=8, markerfacecolor='none', markeredgecolor='k', label='Did not pass quality test', alpha=1)
     
     
    for i,cond in enumerate(set(model_df.Condition)): #plot of every condition, in an individual subplot
        axes[i].set_title(cond,fontsize='x-large') #subtitle
        axes[i].scatter(0,0, s=160000, facecolors='none', edgecolors='k')
        axes[i].scatter(0,0.127, s=1000, marker='^', facecolors='none', edgecolors='k')
        axes[i].set_xlim([-0.10,0.10])
        axes[i].set_ylim([-0.10,0.14])
        axes[i].axis("off")
        axes[i].legend(handles=[redot, blackdot,circle], loc="lower center", bbox_to_anchor=(0.5,-0.05)) #legend
        axis_model_DF_hbo=model_df[(model_df["Condition"]==cond) & (model_df["ch_name"].str[-1]=='o')]
        
        
        #display right colors over each channels
        for chname in axis_model_DF_hbo["ch_name"]:
            chcoord=coordinates._get_ch_pos()[chname]
            hboline=model_df[(model_df["Condition"]==cond) & (model_df["ch_name"]==chname)] #get hbo line from model_df corresponding to current condition and channel name
            hbrline=model_df[(model_df["Condition"]==cond) & (model_df["ch_name"]==chname[:-1]+'r')]
            
            
            #Channel doesn't pass quality test
            if chname[0:-4] in genset_bad_channels :              
                axes[i].scatter(chcoord[0],chcoord[1], s=100, facecolors='none', edgecolors='k')
            
            
            #Activates channels
            elif ((hboline["Coef."].values>0 and hbrline["Coef."].values<0) and (hboline["fdr_p_value"].values<0.05 and hbrline["fdr_p_value"].values<0.05)):
                axes[i].scatter(chcoord[0],chcoord[1], s=200, color='r')
           
            
           #Not activated channels 
            else:
                axes[i].scatter(chcoord[0],chcoord[1], s=100, color='k')
   
    
    plt.show()
    
def plot_2nd_level_res_hbo_and_hbr(fig, axes, model_df, coordinates, genset_bad_channels):
    """
    ----------
    fig : matplotlib.figure.Figure
        DESCRIPTION.
    axes : Array of MatplotlTYPE Axes
        Axe along with every GLM result wil be plot.
    model_df : Dataframe
        Result of second level analysis.
    coordinates : mne.channels.montage.DigMontage
        Coordinates of channels.
    genset_bad_channels : Set
        Set of bad channels included in all subjects.

    Returns
    -------
    None.

    """

    fig.suptitle("Second level GLM",fontsize='xx-large',weight='bold')
    plt.tight_layout(pad=2)
    
    


    #legend definition
    redot=mpl.lines.Line2D([], [], marker='o', color='gray', linestyle='None', markersize=10, label='Activated channel', alpha=1)
    blackdot=mpl.lines.Line2D([], [], marker='X', color='gray', linestyle='None', markersize=8, label='Not activated channel', alpha=1)
    
    hbo_coef_val=model_df[(model_df["ch_name"].str[-1]=='o')]
    hbr_coef_val=model_df[(model_df["ch_name"].str[-1]=='r')]
    coefvalhbo=hbo_coef_val["Coef."].values
    coefvalhbr=hbr_coef_val["Coef."].values
    limhbo=[min(coefvalhbo),max(coefvalhbo)]
    limhbr=[min(coefvalhbr),max(coefvalhbr)]    
    red = plt.cm.YlOrRd(np.linspace(0,1,num=11))
    blue = plt.cm.GnBu_r(np.linspace(0,1,num=11))
    
    for i,cond in enumerate(set(model_df.Condition)): #plot of every condition, in an individual subplot
        axes[i*2].set_title(cond,fontsize='x-large') #subtitle
        axes[i*2].scatter(0,0, s=160000, facecolors='none', edgecolors='k')
        axes[i*2].scatter(0,0.125, s=1000, marker='^', facecolors='none', edgecolors='k')
        axes[i*2].set_xlim([-0.10,0.10])
        axes[i*2].set_ylim([-0.10,0.14])
        axes[i*2].axis("off")
        axes[i*2].legend(handles=[redot, blackdot], loc="lower center", bbox_to_anchor=(0.5,-0.05)) #legend
        plt.colorbar(mpl.cm.ScalarMappable(norm= mpl.colors.Normalize(vmax=limhbo[1],vmin=limhbo[0]), cmap=mpl.cm.YlOrRd), ax=axes[i*2], anchor=(0.0,0.5))
        
        axes[i*2+1].set_title(cond,fontsize='x-large')
        axes[i*2+1].scatter(0,0, s=160000, facecolors='none', edgecolors='k')
        axes[i*2+1].scatter(0,0.125, s=1000, marker='^', facecolors='none', edgecolors='k')
        axes[i*2+1].set_xlim([-0.10,0.10])
        axes[i*2+1].set_ylim([-0.10,0.14])
        axes[i*2+1].axis("off")
        axes[i*2+1].legend(handles=[redot, blackdot], loc="lower center", bbox_to_anchor=(0.5,-0.05)) #legend
        axis_model_DF_hbo=set(hbo_coef_val["ch_name"].values)
        plt.colorbar(mpl.cm.ScalarMappable(norm= mpl.colors.Normalize(vmax=limhbr[1],vmin=limhbr[0]), cmap=mpl.cm.GnBu_r), ax=axes[i*2+1], anchor=(0.0,0.5))
           

        
        #display right colors over each channels
        for chname in axis_model_DF_hbo:
            chcoord=coordinates._get_ch_pos()[chname]
            hboline=model_df[(model_df["Condition"]==cond) & (model_df["ch_name"]==chname)] #get hbo line from model_df corresponding to current condition and channel name
            hbrline=model_df[(model_df["Condition"]==cond) & (model_df["ch_name"]==chname[:-1]+'r')]
            
            
            #Channel doesn't pass quality test
            # if chname[0:-4] in genset_bad_channels :              
            #     axes[i].scatter(chcoord[0],chcoord[1], s=100, facecolors='none', edgecolors='k')
            
            if (hboline["fdr_p_value"].values<0.05) : 
                axes[i*2].scatter(chcoord[0],chcoord[1], s=200, color=red[int(10*(hboline["Coef."].values-limhbo[0])/(limhbo[1]-limhbo[0]))])
            else:
                axes[i*2].scatter(chcoord[0],chcoord[1], s=200, marker="X", color=red[int(10*(hboline["Coef."].values-limhbo[0])/(limhbo[1]-limhbo[0]))])
        
            if (hbrline["fdr_p_value"].values<0.05) : 
                axes[i*2+1].scatter(chcoord[0],chcoord[1], s=200, color=blue[int(10*(hbrline["Coef."].values-limhbr[0])/(limhbr[1]-limhbr[0]))])
            else:
                axes[i*2+1].scatter(chcoord[0],chcoord[1], s=200, marker="X", color=blue[int(10*(hbrline["Coef."].values-limhbr[0])/(limhbr[1]-limhbr[0]))])
                    #Activates channels
                    
    
    plt.show()
        
    
def main():
    

    #gets data from data path to get number of sessions, channels coordinates and annotations
    dataset = mne_bids.BIDSPath(root=pipeline.get_bids_path(), task=mne_bids.get_entity_vals(pipeline.get_bids_path(), 'task')[0], datatype="nirs", suffix="nirs", extension=".snirf")
    subjects = mne_bids.get_entity_vals(pipeline.get_bids_path(), 'subject')
    sessions=mne_bids.get_entity_vals(pipeline.get_bids_path(),'session')
    if (sessions==[]): sessions=[None]
    bids_path=dataset.update(subject=subjects[0], session=sessions[0])
    raw_data = mne_bids.read_raw_bids(bids_path=bids_path, extra_params=None, verbose=True)
    coordinates = mne.preprocessing.nirs.beer_lambert_law(mne.preprocessing.nirs.optical_density(raw_data)).get_montage() #coordinates to plot result at the right space
    annotations=set(raw_data.annotations.description) 
    del raw_data
    
    
    #Delete existing files
    if os.path.exists(pipeline.get_bids_path()+'/derivatives/FRESH-pipeline/Second_Level_Analysis.png'):
        os.remove(pipeline.get_bids_path()+'/derivatives/FRESH-pipeline/Second_Level_Analysis.png')  
    if os.path.exists(pipeline.get_bids_path()+"/derivatives/FRESH-pipeline/SecondLevel_glmDF.pkl"):
        os.remove(pipeline.get_bids_path()+"/derivatives/FRESH-pipeline/SecondLevel_glmDF.pkl")
        

    # open bad channels and put them in a set
    general_bad_channels=list()
    genset_bad_channels=set()
    for sub in subjects:
        with open(pipeline.get_bids_path()+'/derivatives/FRESH-pipeline/sub_'+sub+'_bad_channels.pkl', 'rb') as f:
            general_bad_channels.append(load(f))            
    for idx in range(len(subjects)):
        for j in range(len(general_bad_channels[idx][0])):
            genset_bad_channels.add(general_bad_channels[idx][0][j][:5])
                 

    #download GLM results and put it in a big panda dataframe
    group_glm_DF=pd.DataFrame()
    for sub in subjects:
        group_glm_DF=pd.concat([group_glm_DF, pd.read_pickle(pipeline.get_bids_path()+"/derivatives/FRESH-pipeline/sub_"+sub+"_glmDF.pkl")], ignore_index=True)


    #List names of conditions
    conditionsName=list()
    for cond in set(group_glm_DF.Condition):
        for anno in annotations:
            if anno in cond : conditionsName.append(cond)


    #second level GLM    
    group_res = group_glm_DF.query("Condition in "+str(conditionsName)).copy(deep=True) ## delete groupGLMDF afterward
    #group_res["theta"]=[t*1.e6 for t in group_res["theta"]] # Convert to uM for nicer plotting below
    formule="theta ~ -1 + ch_name:Condition"
    model = smf.mixedlm(formule, group_res, groups=group_res["ID"]).fit(method='nm')   #https://www.statsmodels.org/stable/generated/statsmodels.formula.api.mixedlm.html#statsmodels.formula.api.mixedlm
    
       
    # saving second level glm dataframe
    model_df = mne_nirs.statistics.statsmodels_to_results(model)
    
    #FDR corrected p_values added to DF
    columns_names=model_df.keys().to_list()
    columns_names.append('fdr_p_value')
    final_DF=pd.DataFrame(columns=columns_names)
    for cond in set(model_df.Condition):
        model_DF_hbo=model_df[(model_df["Condition"]==cond) & (model_df["ch_name"].str[-1]=='o')]
        model_DF_hbr=model_df[(model_df["Condition"]==cond) & (model_df["ch_name"].str[-1]=='r')]
        rejectedhbo, corr_p_val_hbo=mne.stats.fdr_correction(model_DF_hbo["P>|z|"])
        rejectedhbr, corr_p_val_hbr=mne.stats.fdr_correction(model_DF_hbr["P>|z|"])
        model_DF_hbo.insert(9,'fdr_p_value',corr_p_val_hbo)
        model_DF_hbr.insert(9,'fdr_p_value',corr_p_val_hbr)
        final_DF=pd.concat([model_DF_hbo,model_DF_hbr,final_DF])
    final_DF.to_pickle(pipeline.get_bids_path()+"/derivatives/FRESH-pipeline/SecondLevel_glmDF.pkl") 
    


    #create figure
    if(plot!='hbo&hbr') : 
        fig, axes = plt.subplots(nrows=1, ncols=len(conditionsName), figsize=(len(conditionsName)*7,7))#,gridspec_kw=dict(width_ratios=[4, 1]))
        plot_2nd_level_res(fig, axes, final_DF, coordinates, genset_bad_channels)
    else: 
        fig, axes = plt.subplots(nrows=1, ncols=len(conditionsName)*2, figsize=(len(conditionsName)*8*2,7))
        plot_2nd_level_res_hbo_and_hbr(fig, axes, final_DF, coordinates, genset_bad_channels)
   
    
    fig.savefig(pipeline.get_bids_path()+'/derivatives/FRESH-pipeline/Second_Level_Analysis-'+plot+'.png')


    #Save pipeline parameters
    parameters = open(pipeline.get_bids_path()+"/derivatives/FRESH-pipeline/parmetersFile.txt","a")
    parameters.write("\nGroup level analysis parameters.\n________________\nDatapath = "+pipeline.get_bids_path()+"\nDate and time = "+str(datetime.datetime.now())+"\nFormule = "+formule)
    parameters.close()
       
        
if __name__ == "__main__":
    
    
    #options for command line running
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description=dedent('''Second level analysis of a dataset. Results will be stores in BidsPath/derivatives/FRESH-pipeline. 
    Must be run after FRESHpipeline-GroupProcessing.py and FERSHpipeline-FirstLevelAnalysis.py'''))
    parser.add_argument('bidsPath', type=str, help='Path to DataFolder in bids format - for Windows "\" must be changes to "/"')
    parser.add_argument('--display','-d', type=bool, default=False, help='Display figure for each subject during processing.')
    parser.add_argument('--plot', type=str, default='conjonction', help="'conjonction' display results taking into account hbo and hbr parameters. 'hbo&hbr' display hbo and hbr results in different plots.")
    args = parser.parse_args()
    display = args.display
    bids_path = args.bidsPath
    plot=args.plot
        
        
    from FRESHpipeline_FirstLevelProcessing import Pipeline
    pipeline=Pipeline.__init__
    with open(bids_path+'/derivatives/FRESH-pipeline/pipeline.pkl', 'rb') as file:
        pipeline = load(file)
        
        
    main() 