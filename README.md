# Readme NIRS processing
## Get NIRx format data to BIDS format 
**why?**
- processing easier using MNE-BIDS (https://mne.tools/mne-bids/stable/) on Python
- Normalize data format (https://bids.neuroimaging.io/) -> makes it shareable

**The script I use :**  Robert Luke script (of course) who was part of the integration of NIRS data to BIDS format https://github.com/rob-luke/fnirs-apps-sourcedata2bids

I use it on Windows so I needed to install wsl: (https://learn.microsoft.com/fr-fr/windows/wsl/basic-commands), helps to execute Bash/Linux commands on Powershell. 
Source data directory need to be manually arrange following this pattern : when there is (XXX) the names structure is important
```
.../SourceDataFolder
└── sourcedata     (XXX)
    ├── sub-01     (XXX)
    │   └── nirs     (XXX)
    │       └── 2020-01-01_001     (the naming of this directory and included files is optional)
    │           ├── NIRS-2020-01-01_001.dat
    │           ├── NIRS-2020-01-01_001.evt
    │           ├── ...
    │
    ├── sub-02     (XXX)
    │   └── nirs     (XXX)
    │       └── 2020-01-02_002     (use the vendor exported format for these directories)
    │           ├── NIRS-2020-01-02_002.dat
    │           ├── NIRS-2020-01-02_002.evt
    │           ├── ...
    :
    :
    └── sub-XX     (XXX)
        └── nirs     (XXX)
            └── 2020-01-05_005
                ├── NIRS-2020-01-05_005.dat
                ├── NIRS-2020-01-05_005.evt
                ├── ...
                └── NIRS-2020-01-05_005_probeInfo.mat
```

```
docker run -v /mnt/c/Users/nojegou/.../SourceDataFolder/:/bids_dataset    ghcr.io/rob-luke/fnirs-apps-sourcedata2bids/app     --task-label motortask     --duration 10     --events "{\"0\":\"Rest\", \"1\":\"Lefthand\", \"2\":\"Righthand\"}"
```
## Open BIDS dataset 
From FRESH study available here : https://osf.io/b4wck/

## Create a virtual environment 
https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/
From this tutorial follow : 
* Creating a virtual environment
* Activating a virtual environment
* Using requirements files
* Leaving the virtual environment

### Creating a virtual environment

Unix/macOS :
```
python3 -m venv env
```
Windows : 
```
py -m venv env
```
### Activating a virtual environment
Unix/macOS :
```
source env/bin/activate
```
Windows : 
```
.\env\Scripts\activate
```
### Using requirements files
Unix/macOS :
```
pip install --upgrade pip
python3 -m pip install -r requirements.txt
```
Windows : 
```
pip install --upgrade pip
py -m pip install -r requirements.txt
```

### Leaving the virtual environment
```
deactivate
```

## First level processing
Script can be found on GitLab : https://gitlab.inria.fr/nojegou/NIRS
```
python FRESHpipeline_FirstLevelProcessing.py ".../DataFolder" 
```
see further options with :
```
python FRESHpipeline_FirstLevelProcessing.py -h
```
options are the following : 
| Option name            | Default value| options | explaination      | exemples         |
|------------------------|--------------|---------|-------------------|------------------|
| --crop, -c    | (None,None)  |      |  Crops data     |  Crop (10,20) will crop data 10 seconds before the first trigger and 20 seconds after the last |
| --quality_check_option |   'sci-psp'  | 'sci-psp', 'snr', 'correlate', None| Quality check method| 'sci-psp' : Scalp coupling index > 0.8 and Peak Spectral Power (PSP) > 0.1, "snr" : Signal to Noise Ratio > 8, "correlate" : hbo and hbr signals must be uncorrelated, None : No quality control is applied |
| --trigger_length   | None  |      | length in seconds of block average use to calcul correlation | When quality check option is 'correlate' : length in seconds of block average use to calcul channel correlation. None : use the default trigger length  |
| --filter_param, -f | (0.01,0.09) from Pinti 2019  | Lowpass filter : [None, High Frequency], Highpass filter : [Low Frequency, None], no filter : [None,None] | Filter parameters | Filter parameters [Low frequency, High frequency]. Width of transition bands are 0.01 each.  |
| --drift_model, -d | None | 'cosine’, ‘polynomial’, None | Specifies the desired drift model in GLM. |   |
|--drift_hp | 0.01 |  | When drift model is cossine : High-pass drift frequency (in Hz).  |   |
| --drift_order | 1 |  | When drift model is polynomial : Order of the drift model. |   |
| --glm_model | 'gen-separate' | 'gen-separate', 'gen-same', 'not-gen-separate' |  'gen-separate' : creates two generalized matrix (where the design matrix of each session is combines in one) one for hbo and one for hbr with corresponding mean of short channels.  'gen-same' creates one generalized matrix used for hbo and hbr with the mean of short channels for hbo and hbr as 2 additional regressors. 'not-gen-separate' create one design matrix without short channel regression for each session and both chromophores.") |   |
| --glm_noise_model| 'arN' | ‘arN’, ‘ols’, ‘auto’, ‘arX’ | The GLM temporal variance model. |  ‘arN’ will replace N by recommended value : 4 times sampling frequency. The AR model can be set to any integer value by modifying the value of X : 'ar1', 'ar2', 'ar100'. |
| --excel | False | True, False  | Save dataframe into excel files for better readability |   |

## First level plot
Script can be found on GitHub : https://gitlab.inria.fr/nojegou/NIRS
```
python FRESHpipeline_FirstLevelPlot.py ".../DataFolder" 
```
option of displaying figures with '-p' or '--PlotFigure'
```
python FRESHpipeline_FirstLevelPlot.py ".../DataFolder" -p True
```
see further options with :
```
python FRESHpipeline-FirstLevelDataAnalysis.py -h
```

options are the following : 
| Option name            | Default value| options | explaination      | exemples         |
|------------------------|--------------|---------|-------------------|------------------|
| --display, -d | False | True, False | Display figure for each subject during processing |  |
| --plot | 'conjonction' | 'conjonction', 'hbo&hbr' | 'conjonction' : display results taking into account hbo and hbr parameters. 'hbo&hbr' display hbo and hbr results in different plots. |  |

## Second level analysis and plot

Script can be found on GitLab : https://gitlab.inria.fr/nojegou/NIRS
```
python FRESHpipeline_SecondLevelAnalysisAndPlot.py ".../DataFolder" 
```
see further options with :
```
python FRESHpipeline_SecondLevelAnalysisAndPlot.py -h
```
options are the following : 
| Option name            | Default value| options | explaination      | exemples         |
|------------------------|--------------|---------|-------------------|------------------|
| --display, -d | False | True, False | Display figure for each subject during processing |  |
| --plot | 'conjonction' | 'conjonction', 'hbo&hbr' | 'conjonction' : display results taking into account hbo and hbr parameters. 'hbo&hbr' display hbo and hbr results in different plots. |  |

