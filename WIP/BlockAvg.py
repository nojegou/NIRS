# -*- coding: utf-8 -*-
"""
Created on Wed Jul  6 09:46:44 2022

@author: nojegou
"""

import mne
import mne_nirs
import mne_bids
import pandas as pd



def individual_analysis(bids_path, ID):
    """
    ID="062"
    BidsPath="C:/Users/nojegou/Documents/Data/IMANES-BIDS"
    #subjects=["055", "056", "061", "062", "075", "076", "081", "082", "083", "091", "092", "106", "107", "111", "112"]
    subjects="055"
    dataset=mne_bids.BIDSPath(root=BidsPath, subject = subjects, task="motortask", datatype="nirs", suffix="nirs", extension=".snirf")
    """
    raw_data = mne_bids.read_raw_bids(bids_path=dataset, extra_params=None, verbose=True)
    #raw_data.annotations.delete(raw_data.annotations.description=="Intro")
    #raw_data.annotations.delete(raw_data.annotations.description=="Restingstate")
    #raw_data.crop(raw_data.annotations.onset[1]+1)
    
    i=0
    RightTrig=False
    print(len(raw_data.get_data()[0,:]))
    print(raw_data.annotations.description)
    print(raw_data.annotations.onset)
    while ((i<len(raw_data.annotations.description)) and not(RightTrig)):
        RightTrig=(raw_data.annotations.description[i]=='Motortask')
        if (RightTrig): raw_data.crop(raw_data.annotations.onset[i]-50,raw_data.annotations.onset[i+3]+45)
        else : raw_data.annotations.delete([i])
    triggerLen = len(raw_data.annotations.description)
    if (triggerLen > 4): raw_data.annotations.delete([-(triggerLen - 4)])
    
    # Convert signal to haemoglobin and resample
    OD = mne.preprocessing.nirs.optical_density(raw_data)
    Conc = mne.preprocessing.nirs.beer_lambert_law(OD,ppf=6)
    #Conc.resample(0.6, verbose=True)
    BPfilt = Conc.copy().filter(0.01, 0.09, l_trans_bandwidth=0.01,h_trans_bandwidth=0.01, method='fir')#BPfilt = Conc.copy().filter(0.01, None, l_trans_bandwidth=0.01, method='fir')
    print(len(raw_data.get_data()[0,:]))
    print("onset 0 = ")
    print(raw_data.annotations.description)
    print(raw_data.annotations.onset)
    print(BPfilt.ch_names[14:16])
    return BPfilt._data[14:16,0:1798], BPfilt.annotations, BPfilt.times[0:1798]

#___________________________________________________________________________________________________________________________________________________________________________
# Group Analysis
BidsPath="C:/Users/nojegou/Documents/Data/IMANES-BIDS-both-session"
dataset = mne_bids.BIDSPath(session="2", root=BidsPath, task="motortask", datatype="nirs", suffix="nirs", extension=".snirf")
subjects = mne_bids.get_entity_vals(BidsPath, 'subject')
import numpy as np
#glm1 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-first-session/GLM-r23/01glm.h5') 

channelS3D3 = np.zeros((18,1798))
i=0
for sub in subjects:
    bids_path = dataset.update(subject=sub)
    print('\n Subject ' + sub + ' : \n')
    channel, trig, time=individual_analysis(bids_path,sub)
    channelS3D3[i:i+2,:]= channel
    print(i)
    i=i+2
    
import matplotlib.pyplot as plt
channel_hbo = channelS3D3[0:18:2,:]
channel_hbr = channelS3D3[1:18:2,:]

stdup=np.mean(np.transpose(channel_hbo),axis=1)+np.std(np.transpose(channel_hbo),axis=1)
stddown=np.mean(np.transpose(channel_hbo),axis=1)-np.std(np.transpose(channel_hbo),axis=1)
plt.fill_between(time,stdup,stddown,alpha=.5, linewidth=0)
plt.plot(time,np.transpose(np.mean(np.transpose(channel_hbo),axis=1)))
plt.vlines(trig.onset[0:4]-trig.onset[0]+50,-3e-6,3e-6)
plt.title("S3_D3 HbO")
plt.show()

stdup=np.mean(np.transpose(channel_hbr),axis=1)+np.std(np.transpose(channel_hbr),axis=1)
stddown=np.mean(np.transpose(channel_hbr),axis=1)-np.std(np.transpose(channel_hbr),axis=1)
plt.fill_between(time,stdup,stddown,alpha=.5, linewidth=0)
plt.plot(time,np.transpose(np.mean(np.transpose(channel_hbr),axis=1)))
plt.vlines(trig.onset[0:4]-trig.onset[0]+50,-3e-6,3e-6)
plt.title("S3_D3 HbR")
plt.show()