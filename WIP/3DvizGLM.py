# -*- coding: utf-8 -*-
"""
Created on Mon Jul  4 15:53:55 2022

@author: nojegou
"""
import os.path as op
import numpy as np
import matplotlib.pyplot as plt
from itertools import compress

import mne
raw_data=mne.io.read_raw_nirx("C:/Users/nojegou/Documents/monProjetIMANES/fNIRS/gr_01/su_08/2020-07-28_002",verbose=True, preload=True)


fnirs_data_folder = mne.datasets.fnirs_motor.data_path()
fnirs_cw_amplitude_dir = op.join(fnirs_data_folder, 'Participant-1')
# = mne.io.read_raw_nirx(fnirs_cw_amplitude_dir, verbose=True)
#raw_intensity.load_data()
raw_data.annotations.set_durations(18)
raw_data.annotations.rename({'1.0': 'Restingstate',
                                  '2.0': 'Intro',
                                  '3.0': 'Motortask'})
#raw_data.annotations.delete(unwanted)
subjects_dir = op.join(mne.datasets.sample.data_path(), 'subjects')

brain = mne.viz.Brain(
    'fsaverage', subjects_dir=subjects_dir, background='w', cortex='0.5')
brain.add_sensors(
    raw_data.info, trans='fsaverage',
    fnirs=['channels', 'pairs', 'sources', 'detectors'])
brain.show_view(azimuth=20, elevation=60, distance=400)