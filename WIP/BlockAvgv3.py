# -*- coding: utf-8 -*-
"""
Created on Wed Jul  6 09:46:44 2022

@author: nojegou
"""

import mne
import mne_nirs
import mne_bids
import pandas as pd



def individual_analysis(bids_path, ID, chnum):

    raw_data = mne_bids.read_raw_bids(bids_path=dataset, extra_params=None, verbose=True)
    i=0
    RightTrig=False
    while ((i<len(raw_data.annotations.description)) and not(RightTrig)):
        RightTrig=(raw_data.annotations.description[i]=='Motortask')
        if (not(RightTrig)): raw_data.annotations.delete([i])
    triggerLen = len(raw_data.annotations.description)
    if (triggerLen > 4): raw_data.annotations.delete([-(triggerLen - 4)])
    
    # Convert signal to haemoglobin and resample
    OD = mne.preprocessing.nirs.optical_density(raw_data)
    OD = mne.preprocessing.nirs.temporal_derivative_distribution_repair(OD)
    Conc = mne.preprocessing.nirs.beer_lambert_law(OD,ppf=6)
    #Conc.resample(0.6, verbose=True)
    BPfilt = Conc.copy().filter(0.01, 0.09, l_trans_bandwidth=0.01,h_trans_bandwidth=0.01, method='fir')

    outputhbo=np.zeros((4,352))
    outputhbr=np.zeros((4,352))
    freq=BPfilt.info['sfreq']
    i=0
    print(BPfilt.ch_names[chnum])
    for tri in BPfilt.annotations.onset :
        if (BPfilt.annotations.description[i]=='Motortask') and i<4:
            outputhbo[i,:]=BPfilt.get_data()[chnum,int(tri*freq):int(tri*freq)+352]
            outputhbr[i,:]=BPfilt.get_data()[chnum+1,int(tri*freq):int(tri*freq)+352]
            i=i+1
    return outputhbo,outputhbr,BPfilt.times

def plotGrpAvg(data,time,title) :
    
    #for each line of hbo need to be substract from mean
    cleandata=data
    i=0
    for epoch in data :
        cleandata[i]=epoch-np.mean(epoch)
        i=i+1
    import matplotlib.pyplot as plt
    stdup=np.mean(cleandata,axis=0)+np.std(cleandata,axis=0)
    stddown=np.mean(cleandata,axis=0)-np.std(cleandata,axis=0)
    mean=np.mean(cleandata,axis=0)
    plt.fill_between(time[0:len(mean)],stdup,stddown,alpha=.5, linewidth=0)
    plt.plot(time[0:len(mean)],mean)
    plt.title(title)
    plt.show()
    
def plotGrpAvgcombined(datahbo,datahbr,time,title,BidsPath) :
    
    #for each line of hbo need to be substract from mean
    cleandatahbo=datahbo
    cleandatahbr=datahbr
    """
    i=0
    for epoch in datahbo :
        cleandatahbo[i]=epoch-np.mean(epoch)
        i=i+1
    i=0
    for epoch in datahbr :
        cleandatahbr[i]=epoch-np.mean(epoch)
        i=i+1  
   """
    import matplotlib.pyplot as plt
    
    meanhbo=np.mean(cleandatahbo,axis=0)
    stdhbo=np.std(cleandatahbo,axis=0)
    stduphbo=meanhbo+stdhbo
    stddownhbo=meanhbo-stdhbo
    
    meanhbr=np.mean(cleandatahbr,axis=0)
    stdhbr=np.std(cleandatahbr,axis=0)
    stduphbr=meanhbr+stdhbr
    stddownhbr=meanhbr-stdhbr
    
    cihbo=1.95 * stdhbo / np.sqrt(len(cleandatahbo))
    cihbr=1.95 * stdhbr / np.sqrt(len(cleandatahbr))
    import matplotlib.collections as collections
    plt.plot(time[0:len(meanhbo)], meanhbo, color='tomato')
    plt.plot(time[0:len(meanhbr)], meanhbr, color='royalblue')
    plt.legend(["HbO", "HbR"])
    plt.fill_between(time[0:len(meanhbo)], meanhbo+cihbo, meanhbo-cihbo, alpha=.5, linewidth=0, color='tomato')
    plt.fill_between(time[0:len(meanhbr)], meanhbr+cihbr ,meanhbr-cihbr, alpha=.5, linewidth=0, color='royalblue')
    plt.axvspan(0,18, facecolor='#2ca02c', alpha=.2,color='k')
    plt.title(title)
    plt.ylim([-1e-6,1e-6])
    
    
    plt.savefig(BidsPath+'/derivatives/FRESH-pipeline/'+title+'.png')
    plt.show()
#___________________________________________________________________________________________________________________________________________________________________________
# Group Analysis
BidsPath="C:/Users/nojegou/Documents/Data/IMANES-BIDS-both-session-only_Motortask"
dataset = mne_bids.BIDSPath(session="2", root=BidsPath, task="motortask", datatype="nirs", suffix="nirs", extension=".snirf")
subjects = mne_bids.get_entity_vals(BidsPath, 'subject')
raw_data = mne_bids.read_raw_bids(bids_path=dataset.update(subject=subjects[0]), extra_params=None, verbose=True)
import numpy as np
#glm1 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-first-session/GLM-r23/01glm.h5') 
from collections import defaultdict
hbo=np.zeros((4*9,352))
hbr=np.zeros((4*9,352))
for chnum in range(0,40,2):
    i=0
    for sub in subjects:
        bids_path = dataset.update(subject=sub)
        outputhbo, outputhbr, time=individual_analysis(bids_path,sub,chnum)
        hbo[i:i+4,:]=outputhbo
        hbr[i:i+4,:]=outputhbr
        i=i+4
    plotGrpAvgcombined(hbo,hbr,time,"Block_average_"+raw_data.ch_names[chnum][0:5]+"-tddr-corr",BidsPath)
