# -*- coding: utf-8 -*-
"""
Created on Fri Jul  1 09:52:48 2022

@author: nojegou
"""
import mne
import mne_nirs
import numpy as np
#plot individual HbO GLM
import mne_bids
"""
glm1 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-first-session/GLM-r26/01glm.h5') 
glm4 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-first-session/GLM-r26/04glm.h5') 
glm5 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-first-session/GLM-r26/05glm.h5') 
glm6 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-first-session/GLM-r26/06glm.h5') 
glm7 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-first-session/GLM-r26/07glm.h5') 
glm8 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-first-session/GLM-r26/08glm.h5') 
glm9 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-first-session/GLM-r26/09glm.h5') 
glm10 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-first-session/GLM-r26/10glm.h5') 
glm11 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-first-session/GLM-r26/11glm.h5')
"""
"""
glm1 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-second-session/GLM-r26/01glm.h5') 
glm4 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-second-session/GLM-r26/04glm.h5') 
glm5 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-second-session/GLM-r26/05glm.h5') 
glm6 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-second-session/GLM-r26/06glm.h5') 
glm7 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-second-session/GLM-r26/07glm.h5') 
glm8 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-second-session/GLM-r26/08glm.h5') 
glm9 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-second-session/GLM-r26/09glm.h5') 
glm10 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-second-session/GLM-r26/10glm.h5') 
glm11 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-second-session/GLM-r26/11glm.h5')
"""
folderpath='C:/Users/nojegou/Documents/Data/IMANES-BIDS-both-session/derivatives/crop-ar-glm/'   #--------------------filepath
BidsPath="C:/Users/nojegou/Documents/Data/IMANES-BIDS-both-session"
subjects = mne_bids.get_entity_vals(BidsPath, 'subject')
session=mne_bids.get_entity_vals(BidsPath,'session')
#get min and max of data to plot every subjects on same scale
minData=0
maxData=0
for sess in session:
    for sub in subjects:
        #path=folderpath+'sub'+sub+'-sess'+sess+"crop-ar-glm.h5"                                  #--------------------filepath
        path="C:/Users/nojegou/Documents/Data/Elise-workshop/eliseglm.h5"
        glm=mne_nirs.statistics.read_glm(path)
        path=''
        theta=glm.copy().to_dataframe().query('Condition in ["Motortask"]').t.to_list()
        theta=theta[0:40:2]
        theta.sort()
        if(minData>theta[0]) : minData=theta[0]
        if(maxData<theta[-1]) : maxData=theta[-1]
print("min = "+str(minData))
print("max = "+str(maxData))
#plot
for sess in session:
    for sub in subjects:
        #path=folderpath+'sub'+sub+'-sess'+sess+"crop-ar-glm.h5"                                  #--------------------filepath
        path="C:/Users/nojegou/Documents/Data/Elise-workshop/eliseglm.h5"
        glm=mne_nirs.statistics.read_glm(path)
        path=''
        theta=glm.copy().to_dataframe().query('Condition in ["Motortask"]').t.to_list() 
        import matplotlib.pyplot as plt
        import matplotlib as mpl
        sublist=["1","4","5","6","7","8","9","10","11"]
        probe=["S1","D1","S2","D3","S1","D2","S3","D3","S4","D4","S2","S5","D5","S6","D7","S5","D6","S7","D7","S8","D8","S6",]
        chOrder=[17,6,8,4,2,12,14,16,18,10,0,20,26,28,24,22,32,34,36,38,30]
        x=[3,3,2,2,3,3,2,2,1,1,2,4,4,5,5,4,4,5,5,6,6,5]
        y=[2,3,3,2,2,1,1,2,2,3,3,2,3,3,2,2,1,1,2,2,3,3]
        stat=np.zeros((40,1))
        stat=glm.copy().to_dataframe().query('Condition in ["Motortask"]').p_value.to_list()
        colors = plt.cm.YlOrRd(np.linspace(0,1,num=11))
        i=0
        while i<len(x)-1:
            plt.text(x[i],y[i],probe[i])
            i=i+1
        i=0
        print("subject : "+sub)
        
        while(i<len(x)-1):
            if((probe[i]=="S2") and (probe[i+1]=="S5")): 
                i=i+1
            CurrTheta=theta[chOrder[i]]
            CurrStat=stat[chOrder[i]]
            if( CurrStat<0.0025):
                plt.plot([x[i],x[i+1]],[y[i],y[i+1]],color=colors[int(10*((CurrTheta-minData)/(maxData-minData)))],linewidth=5) #plt.plot([x[i],x[i+1]],[y[i],y[i+1]],color=colors[int(10*((CurrTheta-theta[0:40:2].min())/theta[0:40:2].max()))],linewidth=5)   #
            else:plt.plot([x[i],x[i+1]],[y[i],y[i+1]],color=colors[int(10*((CurrTheta-minData)/(maxData-minData)))],linewidth=5, dashes=[4, 2]) #plt.plot([x[i],x[i+1]],[y[i],y[i+1]],color=colors[int(10*((CurrTheta-theta[0:40:2].min())/theta[0:40:2].max()))],linewidth=5)   #
            i=i+1
            #print((CurrTheta-maxData))
            #print(int(10*((CurrTheta-max)/(min-max))))
        plt.axis('off')   
        plt.axis('equal')
        plt.title("GLM "+sub+" hbo - session "+sess)
            
        plt.colorbar(mpl.cm.ScalarMappable(norm= mpl.colors.Normalize(vmax=maxData,vmin=minData), cmap=mpl.cm.YlOrRd), label='t')
        plt.show()
"""
glm1 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-both-session/derivatives/second-session-OLS-GLM/01glm.h5') 
glm4 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-both-session/derivatives/second-session-OLS-GLM/04glm.h5') 
glm5 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-both-session/derivatives/second-session-OLS-GLM/05glm.h5') 
glm6 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-both-session/derivatives/second-session-OLS-GLM/06glm.h5') 
glm7 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-both-session/derivatives/second-session-OLS-GLM/07glm.h5') 
glm8 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-both-session/derivatives/second-session-OLS-GLM/08glm.h5') 
glm9 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-both-session/derivatives/second-session-OLS-GLM/09glm.h5') 
glm10 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-both-session/derivatives/second-session-OLS-GLM/10glm.h5') 
glm11 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-both-session/derivatives/second-session-OLS-GLM/11glm.h5')

#raw_data=mne.io.read_raw_nirx("C:/Users/nojegou/Documents/monProjetIMANES/fNIRS/gr_01/su_08/2020-07-28_002",verbose=True, preload=True)
theta=np.zeros((40,9))
 
theta[:,0]=glm1.copy().to_dataframe().query('Condition in ["Motortask"]').t.to_list()  
theta[:,1]=glm4.copy().to_dataframe().query('Condition in ["Motortask"]').t.to_list()
theta[:,2]=glm5.copy().to_dataframe().query('Condition in ["Motortask"]').t.to_list()
theta[:,3]=glm6.copy().to_dataframe().query('Condition in ["Motortask"]').t.to_list()  
theta[:,4]=glm7.copy().to_dataframe().query('Condition in ["Motortask"]').t.to_list()
theta[:,5]=glm8.copy().to_dataframe().query('Condition in ["Motortask"]').t.to_list()
theta[:,6]=glm9.copy().to_dataframe().query('Condition in ["Motortask"]').t.to_list()
theta[:,7]=glm10.copy().to_dataframe().query('Condition in ["Motortask"]').t.to_list()
theta[:,8]=glm11.copy().to_dataframe().query('Condition in ["Motortask"]').t.to_list()
#min and max for HbO first session min = -12.088015503156155 & max = 55.606321507113385 / second session min = -13.35712597618235 & max = 67.1068077937166
min=-13.35712597618235#np.min(theta)
max=67.1068077937166#np.max(theta)

import matplotlib.pyplot as plt
import matplotlib as mpl
glmlist=[glm1, glm4, glm5, glm6, glm7, glm8, glm9, glm10, glm11]
sublist=["1","4","5","6","7","8","9","10","11"]
sub=0
for glm in (glmlist):
    probe=["S1","D1","S2","D3","S1","D2","S3","D3","S4","D4","S2","S5","D5","S6","D7","S5","D6","S7","D7","S8","D8","S6",]
    chOrder=[0,6,8,4,2,12,14,16,18,10,0,20,26,28,24,22,32,34,36,38,30]
    x=[3,3,2,2,3,3,2,2,1,1,2,4,4,5,5,4,4,5,5,6,6,5]
    y=[2,3,3,2,2,1,1,2,2,3,3,2,3,3,2,2,1,1,2,2,3,3]
    tpatient=theta[:,sub]
    #tpatient=glm.copy().to_dataframe().query('Condition in ["Motortask"]').t.to_list()
    stat=np.zeros((40,1))
    stat=glm.copy().to_dataframe().query('Condition in ["Motortask"]').p_value.to_list()
    colors = plt.cm.YlOrRd(np.linspace(0,1,num=11))
    i=0
    while i<len(x)-1:
        plt.text(x[i],y[i],probe[i])
        i=i+1
    i=0
    print("subject : "+str(sub))
    
    while(i<len(x)-1):
        if((probe[i]=="S2") and (probe[i+1]=="S5")): 
            i=i+1
        CurrTheta=tpatient[chOrder[i]]
        CurrStat=stat[chOrder[i]]
        if( CurrStat<0.0025):
            plt.plot([x[i],x[i+1]],[y[i],y[i+1]],color=colors[int(10*((CurrTheta-min)/(max-min)))],linewidth=5) #plt.plot([x[i],x[i+1]],[y[i],y[i+1]],color=colors[int(10*((CurrTheta-theta[0:40:2].min())/theta[0:40:2].max()))],linewidth=5)   #
        else:plt.plot([x[i],x[i+1]],[y[i],y[i+1]],color=colors[int(10*((CurrTheta-min)/(max-min)))],linewidth=5, dashes=[4, 2]) #plt.plot([x[i],x[i+1]],[y[i],y[i+1]],color=colors[int(10*((CurrTheta-theta[0:40:2].min())/theta[0:40:2].max()))],linewidth=5)   #
        i=i+1
        print((CurrTheta-max))
        #print(int(10*((CurrTheta-max)/(min-max))))
    plt.axis('off')   
    plt.axis('equal')
    plt.title("GLM "+sublist[sub]+" hbo - second session")
    sub=sub+1
    
    plt.colorbar(mpl.cm.ScalarMappable(norm= mpl.colors.Normalize(vmax=max,vmin=min), cmap=mpl.cm.YlOrRd), label='t')
    plt.show()

#plot individual HbR GLM
"""
"""
glm1 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-first-session/GLM-r26/01glm.h5') 
glm4 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-first-session/GLM-r26/04glm.h5') 
glm5 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-first-session/GLM-r26/05glm.h5') 
glm6 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-first-session/GLM-r26/06glm.h5') 
glm7 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-first-session/GLM-r26/07glm.h5') 
glm8 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-first-session/GLM-r26/08glm.h5') 
glm9 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-first-session/GLM-r26/09glm.h5') 
glm10 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-first-session/GLM-r26/10glm.h5') 
glm11 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-first-session/GLM-r26/11glm.h5')

glm1 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-second-session/GLM-r26/01glm.h5') 
glm4 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-second-session/GLM-r26/04glm.h5') 
glm5 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-second-session/GLM-r26/05glm.h5') 
glm6 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-second-session/GLM-r26/06glm.h5') 
glm7 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-second-session/GLM-r26/07glm.h5') 
glm8 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-second-session/GLM-r26/08glm.h5') 
glm9 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-second-session/GLM-r26/09glm.h5') 
glm10 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-second-session/GLM-r26/10glm.h5') 
glm11 = mne_nirs.statistics.read_glm('C:/Users/nojegou/Documents/Data/IMANES-BIDS-second-session/GLM-r26/11glm.h5')

#raw_data=mne.io.read_raw_nirx("C:/Users/nojegou/Documents/monProjetIMANES/fNIRS/gr_01/su_08/2020-07-28_002",verbose=True, preload=True)
theta=np.zeros((40,9))

theta[:,0]=glm1.copy().to_dataframe().query('Condition in ["Motortask"]').t.to_list()
theta[:,1]=glm4.copy().to_dataframe().query('Condition in ["Motortask"]').t.to_list()
theta[:,2]=glm5.copy().to_dataframe().query('Condition in ["Motortask"]').t.to_list() 
theta[:,3]=glm6.copy().to_dataframe().query('Condition in ["Motortask"]').t.to_list()
theta[:,4]=glm7.copy().to_dataframe().query('Condition in ["Motortask"]').t.to_list()
theta[:,5]=glm8.copy().to_dataframe().query('Condition in ["Motortask"]').t.to_list()
theta[:,6]=glm9.copy().to_dataframe().query('Condition in ["Motortask"]').t.to_list()
theta[:,7]=glm10.copy().to_dataframe().query('Condition in ["Motortask"]').t.to_list()
theta[:,8]=glm11.copy().to_dataframe().query('Condition in ["Motortask"]').t.to_list()
#min and max for HbR first session min = -45.38139769409759 & max = 30.050953530957376 / second session min = -52.90561069084615 & max = 17.714269561896057
#min and maw accross sessions min = -52.90561069084615 & max = 30.050953530957376
min=-52.90561069084615 #for homogeneity throughout outputs np.min(theta[1:40:2])
max=30.050953530957376 #for homogeneity throughout outputs np.max(theta[1:40:2])
import matplotlib.pyplot as plt
import matplotlib as mpl
glmlist=[glm1, glm4, glm5, glm6, glm7, glm8, glm9, glm10, glm11]
sublist=["1","4","5","6","7","8","9","10","11"]
sub=0
for glm in (glmlist):
    probe=["S1","D1","S2","D3","S1","D2","S3","D3","S4","D4","S2","S5","D5","S6","D7","S5","D6","S7","D7","S8","D8","S6",]
    chOrder=[0,6,8,4,2,12,14,16,18,10,0,20,26,28,24,22,32,34,36,38,30]
    x=[3,3,2,2,3,3,2,2,1,1,2,4,4,5,5,4,4,5,5,6,6,5]
    y=[2,3,3,2,2,1,1,2,2,3,3,2,3,3,2,2,1,1,2,2,3,3]
    tPatient=theta[:,sub]
    #tPatient=glm.copy().to_dataframe().query('Condition in ["Motortask"]').t.to_list()
    stat=np.zeros((40,1))
    stat=glm.copy().to_dataframe().query('Condition in ["Motortask"]').p_value.to_list()
    colors = plt.cm.GnBu(np.linspace(0,1,num=11))
    i=0
    while i<len(x)-1:
        plt.text(x[i],y[i],probe[i])
        i=i+1
    i=0
    while(i<len(x)-1):
        if((probe[i]=="S2") and (probe[i+1]=="S5")): 
            i=i+1
        print("i = "+str(i))
        print("channel order = "+str(chOrder[i]))
        CurrTheta=tPatient[chOrder[i]+1]
        CurrStat=stat[chOrder[i]+1]
        if( CurrStat<0.0025):
            plt.plot([x[i],x[i+1]],[y[i],y[i+1]],color=colors[int(10*((CurrTheta-max)/(min-max)))],linewidth=5) #plt.plot([x[i],x[i+1]],[y[i],y[i+1]],color=colors[int(10*((CurrTheta-theta[0:40:2].min())/theta[0:40:2].max()))],linewidth=5)   #
        else:plt.plot([x[i],x[i+1]],[y[i],y[i+1]],color=colors[int(10*((CurrTheta-max)/(min-max)))],linewidth=5, dashes=[4, 2]) #plt.plot([x[i],x[i+1]],[y[i],y[i+1]],color=colors[int(10*((CurrTheta-theta[0:40:2].min())/theta[0:40:2].max()))],linewidth=5)   #
        i=i+1
 
    plt.axis('off')
    plt.axis('equal')
    plt.title("GLM "+sublist[sub]+" hbr - second session")
    sub=sub+1
    
    plt.colorbar(mpl.cm.ScalarMappable(norm= mpl.colors.Normalize(vmax=min,vmin=max), cmap=mpl.cm.GnBu.reversed()), label='t')
    plt.show()
  """  
#_____________________________________________________________________________________________________
#Second Level GLM
"""
import numpy as np
coeff=np.zeros((40,1))
xcoeff=np.zeros((40,1))
with open('C:/Users/nojegou/Documents/Data/IMANES-BIDS-First-session/derivatives/fnirs-apps-glm-pipeline-filter-design-crop-ROI-trigger-ch2lv/stats.txt') as f:
    lines = f.readlines()
import matplotlib.pyplot as plt
import numpy as np
#colors = plt.cm.Blues(np.linspace(0,1,num=11))      #hbR plot
probe=["S1","D1","S2","D3","S1","D2","S3","D3","S4","D4","S2","S5","D5","S6","D7","S5","D6","S7","D7","S8","D8","S6",]
chOrder=[0,6,8,4,2,12,14,16,18,10,0,20,26,28,24,22,32,34,36,38,30]
x=[3,3,2,2,3,3,2,2,1,1,2,4,4,5,5,4,4,5,5,6,6,5]
y=[2,3,3,2,2,1,1,2,2,3,3,2,3,3,2,2,1,1,2,2,3,3]
fig, ax = plt.subplots()
for i in range (0,40): coeff[i]=lines[11+i].split()[2]
#for i in range (0,40): coeff[i]=coeff[i]-coeff[1:40:2].max()                        #hbR plot
i=0
while i<len(x)-1:
    plt.scatter(x[i],y[i])
    plt.text(x[i],y[i],probe[i])
    i=i+1
i=0
while(i<len(x)-1):
    if((probe[i]=="S2") and (probe[i+1]=="S5")): 
        i=i+1
    CurrTheta=coeff[chOrder[i]]                        #hbO plot
    #CurrTheta=coeff[chOrder[i]+1]                        #hbR plot
    plt.plot([x[i],x[i+1]],[y[i],y[i+1]],color=colors[int(10*((CurrTheta-coeff[0:40:2].min())/coeff[0:40:2].max()))],linewidth=5)                  #hbO plot
    print(CurrTheta)
    print(int(10*(CurrTheta/coeff[1:40:2].min())))                                                                              #hbR plot
    #plt.plot([x[i],x[i+1]],[y[i],y[i+1]],color=colors[int(10*(CurrTheta/coeff[1:40:2].min()))],linewidth=5)                    #hbR plot
    i=i+1
plt.axis('off')   
plt.title("Second Level GLM - First session")
plt.show()
"""
#Plot second level with functions HBO isn't working
"""
def plotHBO(lines,title):
    import numpy as np
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    coeff=np.zeros((40,1))
    pvalue=np.zeros((40,1))
    colors = plt.cm.YlOrRd(np.linspace(0,1,num=11))           #hbO plot
    probe=["S1","D1","S2","D3","S1","D2","S3","D3","S4","D4","S2","S5","D5","S6","D7","S5","D6","S7","D7","S8","D8","S6",]
    chOrder=[0,6,8,4,2,12,14,16,18,10,0,20,26,28,24,22,32,34,36,38,30]
    x=[3,3,2,2,3,3,2,2,1,1,2,4,4,5,5,4,4,5,5,6,6,5]
    y=[2,3,3,2,2,1,1,2,2,3,3,2,3,3,2,2,1,1,2,2,3,3]
    fig, ax = plt.subplots()
    for i in range (0,40): 
        coeff[i]=lines[11+i].split()[2]
        pvalue[i]=lines[11+i].split()[5]
    i=0
    while i<len(x)-1:
        plt.scatter(x[i],y[i])
        plt.text(x[i],y[i],probe[i])
        i=i+1
    i=0
    max=0.294#np.max(coeff[0:40:2]) # max for homogeneity throuhout prints 0.294
    while(i<len(x)-1):
        if((probe[i]=="S2") and (probe[i+1]=="S5")): 
            i=i+1
        CurrTheta=coeff[chOrder[i]]                        #hbO plot
        CurrPvalue=pvalue[chOrder[i]] 
        if CurrPvalue<0.0025 : plt.plot([x[i],x[i+1]],[y[i],y[i+1]],color=colors[int(10*((CurrTheta)/max))],linewidth=5)                  #hbO plot
        else: plt.plot([x[i],x[i+1]],[y[i],y[i+1]],color=colors[int(10*((CurrTheta)/max))],linewidth=5,dashes=[4, 2])
        print(CurrTheta)
        i=i+1
    plt.axis('off')   
    plt.title(title)
    plt.colorbar(mpl.cm.ScalarMappable(norm= mpl.colors.Normalize(vmax=max,vmin=0), cmap=mpl.cm.YlOrRd), label='HbO')
    plt.axis('equal')
    plt.savefig(title+'hbo.png', dpi=300)
    plt.show()
    
def plotHBR(lines,title):
    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    coeff=np.zeros((40,1))
    pvalue=np.zeros((40,1))
    colors = plt.cm.GnBu(np.linspace(0,1,num=11))  
    probe=["S1","D1","S2","D3","S1","D2","S3","D3","S4","D4","S2","S5","D5","S6","D7","S5","D6","S7","D7","S8","D8","S6",]
    chOrder=[0,6,8,4,2,12,14,16,18,10,0,20,26,28,24,22,32,34,36,38,30]
    x=[3,3,2,2,3,3,2,2,1,1,2,4,4,5,5,4,4,5,5,6,6,5]
    y=[2,3,3,2,2,1,1,2,2,3,3,2,3,3,2,2,1,1,2,2,3,3]
    fig, ax = plt.subplots()
    for i in range (0,40): 
        coeff[i]=lines[11+i].split()[2]
        pvalue[i]=lines[11+i].split()[5]      
    i=0
    while i<len(x)-1:
        plt.scatter(x[i],y[i])
        plt.text(x[i],y[i],probe[i])
        i=i+1
    i=0
    min=-0.218 #for homogenous results np.min(coeff[1:40:2])
    max=np.max(coeff[1:40:2])
    print(min)
    while(i<len(x)-1):
        if((probe[i]=="S2") and (probe[i+1]=="S5")): 
            i=i+1
        CurrTheta=coeff[chOrder[i]+1]  
        print(CurrTheta)
        CurrPvalue=pvalue[chOrder[i]+1] 
        print("couleur : "+str(10*(CurrTheta-max)/min))
        if CurrPvalue<0.0025 : plt.plot([x[i],x[i+1]],[y[i],y[i+1]],color=colors[int(10*((CurrTheta-max)/(min-max)))],linewidth=5)                  #hbO plot
        else: plt.plot([x[i],x[i+1]],[y[i],y[i+1]],color=colors[int(10*((CurrTheta-max)/(min-max)))],linewidth=5,dashes=[4, 2])
        #plt.plot([x[i],x[i+1]],[y[i],y[i+1]],color=colors[int((CurrTheta/coeff[1:40:2].min()))],linewidth=5)          
        i=i+1
    plt.axis('off')   
    plt.title(title)
    plt.axis('equal')
    plt.colorbar(mpl.cm.ScalarMappable(norm= mpl.colors.Normalize(vmax=min,vmin=0), cmap=mpl.cm.GnBu.reversed()), label='HbR')
    plt.savefig(title+'hbr.png', dpi=300)
    plt.show()    
    
    

with open('C:/Users/nojegou/Documents/Data/IMANES-BIDS-first-session/derivatives/fnirs-apps-glm-pipeline-filter-design-crop-ROI-trigger-ch2lv/stats.txt') as f:
    lines = f.readlines()
    plotHBO(lines,"First session - Second level GLM")
    plotHBR(lines,"First session - Second level GLM")
with open('C:/Users/nojegou/Documents/Data/IMANES-BIDS-second-session/derivatives/fnirs-apps-glm-pipeline-filter-design-crop-ROI-trigger-ch2lv/stats.txt') as f:
    lines = f.readlines()
    plotHBO(lines,"Second session - Second level GLM")
    plotHBR(lines,"Second session - Second level GLM")
with open('C:/Users/nojegou/Documents/Data/IMANES-BIDS-both-session/derivatives/fnirs-apps-glm-pipeline-filter-design-crop-ROI-trigger-ch2lv/stats.txt') as f:
     lines = f.readlines()
     plotHBO(lines,"Both sessions - Second level GLM")
     plotHBR(lines,"Both sessions - Second level GLM")

with open('C:/Users/nojegou/Documents/Data/IMANES-BIDS-first-session/derivatives/fnirs-apps-glm-pipeline-filter-design-crop-ROI-trigger-ch2lv/stats.txt') as f:
     lines = f.readlines()
     plotHBO(lines,"First sessions - Second level GLM")
with open('C:/Users/nojegou/Documents/Data/IMANES-BIDS-second-session/derivatives/fnirs-apps-glm-pipeline-filter-design-crop-ROI-trigger-ch2lv/stats.txt') as f:
     lines = f.readlines()
     plotHBO(lines,"Second sessions - Second level GLM")
with open('C:/Users/nojegou/Documents/Data/IMANES-BIDS-both-session/derivatives/fnirs-apps-glm-pipeline-filter-design-crop-ROI-trigger-ch2lv/stats.txt') as f:
     lines = f.readlines()
     plotHBO(lines,"Both sessions - Second level GLM")

     """