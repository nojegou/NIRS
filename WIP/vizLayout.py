# -*- coding: utf-8 -*-
"""
Created on Thu Jan 19 14:41:35 2023

@author: nojegou
"""

import mne
import matplotlib.pyplot as plt
import pandas as pd 
import numpy as np

#get_layout('C:/Users/nojegou/Documents/Data/FRESH/FRESH-Motor/FRESH-Motor/sub-01/ses-left2s/nirs/sub-01_ses-left2s_optodes.tsv','C:/Users/nojegou/Documents/Data/FRESH/FRESH-Motor/FRESH-Motor/sub-01/ses-left2s/nirs/sub-01_ses-left2s_task-FRESHMOTOR_channels.tsv',14)
    
def get_layout(optodestsv, channelstsv, source):
    """   Plot montage layout
    Parameters
    ----------
    %(info_not_none)s
    optodestsv : str 
        Filepath to optodes.tsv file
    channelstsv : str 
        Filepath to channels.tsv file
    source : int 
        Nombre de source dans le layout
        
    Returns
    ----------
    A figure of the probe layout
    """
    #optodes=pd.read_csv('C:/Users/nojegou/Documents/Data/FRESH/FRESH-Motor/FRESH-Motor/sub-01/ses-left2s/nirs/sub-01_ses-left2s_optodes.tsv',sep='\s+')
    optodes=pd.read_csv(optodestsv,sep='\s+') #create a dataframe with the source and detector name and their placement
    fig, axes = plt.subplots()
    for i in range(len(optodes)):
        axes.text(optodes.x[i],optodes.y[i],optodes.name[i]) #plot source and detector name
    axes.scatter(optodes[0:source+1].x,optodes[0:source+1].y,c='r') #plot source position
    axes.scatter(optodes[source+1:len(optodes)].x,optodes[source+1:len(optodes)].y,c='b') #plot detector position
    channels=pd.read_csv(channelstsv,sep='\s+')
    channels=channels.index[0:len(channels):2].tolist()
    for i in range(len(channels)):
        p1=get_optode_pos(channels[i][3], optodes)
        p2=get_optode_pos(channels[i][4], optodes)
        axes.plot([p1[0],p2[0]],[p1[1],p2[1]],c='k')
    axes.axis('off')       
    
    
    
def get_optode_pos(name, optodes):
    """   get optode x and y position
    Parameters
    ----------
    %(info_not_none)s
    name : str 
        name of the optode 'SX' (for source) or 'DX' (for detector)
    optodes : DataFrame
        dataframe of the optode name and placement
    """
    for i in range(len(optodes)):
        if optodes.name[i]==name:
            return (optodes.x[i],optodes.y[i])
    return "error"