# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 12:32:45 2022
This script is a work in progress to understand GLM
@author: nojegou
"""
import mne_nirs
import mne
import matplotlib.pyplot as plt

raw_data=mne.io.read_raw_nirx("C:/Users/nojegou/Documents/monProjetIMANES/fNIRS/gr_01/su_08/2020-07-28_002",verbose=True, preload=True)
"""
Annotations are deleted so only motor task are used in the GLM
"""
raw_data.annotations.rename({'1.0': 'Resting state', '2.0': 'Introduction', '3.0': 'Motor task'})
raw_data.annotations.delete([0,1])
"""
In case data needs to be cropped
"""
#raw_data=raw_data.copy().crop(raw_data.annotations.onset[1]+1,raw_data.times[len(raw_data.times)-1])

tstart = raw_data.annotations.onset[1]

OD=mne.preprocessing.nirs.optical_density(raw_data.copy())
#OD._data=OD._data*1e9   #If data needs to be compared with Matlab
Conc=mne.preprocessing.nirs.beer_lambert_law(OD,ppf=6)  #to have a comparison base with matlab use  : Conc=mne.preprocessing.nirs.beer_lambert_law(OD,ppf=6*1000)
BPfilt=Conc.copy().filter(0.01, 0.09, l_trans_bandwidth=0.009,h_trans_bandwidth=0.01, method='fir')

#___________________________________________________________________________________________________________________________________________________________
### GLM    
"""
high_pass parameter must be specified per experiment , the cutoff period (1/high_pass) should be set as the longest period between two trials of the same condition multiplied by 2.
For instance, if the longest period is 32s, the high_pass frequency shall be 1/64 Hz ~ 0.016 Hz"`
"""
design_matrix = mne_nirs.experimental_design.make_first_level_design_matrix(BPfilt, drift_model='cosine', high_pass=0.0111, hrf_model='glover', stim_dur=18)
fig, ax1 = plt.subplots(figsize=(10, 6), nrows=1, ncols=1)
from nilearn.plotting import plot_design_matrix
fig = plot_design_matrix(design_matrix, ax=ax1)
glm_est =  mne_nirs.statistics.run_glm(BPfilt, design_matrix)
glm_est.plot_topo(conditions=['Motor task'])

#Left-Right GLM dataframe
left = [[6, 5], [6, 8], [5, 5], [6, 7], [8, 8], [5, 7], [8, 7], [5, 6], [7, 7], [7, 6]]
right = [[2, 4], [2, 1], [4, 4], [2, 3], [1, 1], [4, 3], [1, 3], [3, 3], [1, 2], [3, 2]]
groups = dict(
    Left_Hemisphere=mne_nirs.channels.picks_pair_to_idx(BPfilt, left, on_missing='ignore'),
    Right_Hemisphere=mne_nirs.channels.picks_pair_to_idx(BPfilt, right, on_missing='ignore'))

lrglm = glm_est.to_dataframe_region_of_interest(groups, design_matrix.columns, demographic_info=True)

#___________________________________________________________________________________________________________________________________________________________
### Second level GLM  
"""
import pandas as pd
import statsmodels
df_cha = pd.DataFrame()  # To store channel level results

df_cha = glm_est.copy().to_dataframe()
ch_summary = df_cha.query("Condition in ['Motor task']")
assert len(ch_summary)
ch_summary = ch_summary.query("Chroma in ['hbo']")
ch_model = statsmodels.formula.api.mixedlm("theta ~ -1 + ch_name", ch_summary,
                                          ).fit(method='nm')
model_df = mne_nirs.statistics.statsmodels_to_results(ch_model, order=raw_data.copy().pick("hbo").ch_names)
mne_nirs.visualisation.plot_glm_surface_projection(BPfilt.copy(),glm_est.to_dataframe(),picks='hbo')
"""

"""
glmdf=glm_est.to_dataframe()     
glmdf=glmdf.drop(['df','mse','p_value','se','t','Source','Detector','Significant'],axis=1)
glmdf[(glmdf["Condition"]=='Resting state')|(glmdf["Condition"]=='Motor task')]
glmdfChroma=(glmdf.query('Condition in ["Resting state", "Introduction", "Motor task"]').groupby(['Condition', 'Chroma', 'ch_name']).agg(['mean']))
"""
#glm_est.scatter()
#___________________________________________________________________________________________________________________________________________________________
#GLM différencié HbO & HbR
"""
#BPfilt.crop(raw_data.annotations.onset[2]-20,raw_data.times[len(raw_data.times)-1])
hbofilt=BPfilt.copy().pick('hbo')
hbrfilt=BPfilt.copy().pick('hbr')
design_matrixhbo = mne_nirs.experimental_design.make_first_level_design_matrix(BPfilt, drift_model='cosine', high_pass=0.0111, hrf_model='glover', stim_dur=18)#, drift_order=5)  
glmhbo_est = mne_nirs.statistics.run_glm(hbofilt, design_matrixhbo)
design_matrixhbr = mne_nirs.experimental_design.make_first_level_design_matrix(BPfilt, drift_model='cosine', high_pass=0.0111, hrf_model='glover', stim_dur=18)#, drift_order=5)  
glmhbr_est = mne_nirs.statistics.run_glm(hbrfilt, design_matrixhbr)
"""

#GLM comparison 
"""
#hrf_model alternatives : ‘glover’, ‘spm’, ‘spm + derivative’, ‘spm + derivative + dispersion’, ‘glover + derivative’, ‘glover + derivative + dispersion’, ‘fir’,
drift_model='cosine'
hrf_model='glover'
design_matrix = mne_nirs.experimental_design.make_first_level_design_matrix(BPfilt, drift_model=drift_model, high_pass=0.0111, hrf_model=hrf_model, stim_dur=18, drift_order=10)  
#fig, ax1 = plt.subplots(figsize=(10, 6), nrows=1, ncols=1)
#from nilearn.plotting import plot_design_matrix
#fig = plot_design_matrix(design_matrix, ax=ax1)
glm_est = mne_nirs.statistics.run_glm(BPfilt, design_matrix)
"""
"""
glm_est.plot_topo(conditions=['Motor task'])
plt.show()
plt.figure(figsize=(12,8))
plt.plot(glm_est.ch_names,glm_est.MSE())
plt.xticks(rotation=45, ha='right',fontsize='small')
plt.title("MSE drift model : "+drift_model+", hrf model : "+hrf_model)
plt.show()
import numpy
beta=numpy.zeros(40)
for i in range(0,40):
    beta[i]=glm_est.theta()[i][0]
plt.figure(figsize=(12,8))
plt.plot(glm_est.ch_names,beta)
plt.xticks(rotation=45, ha='right',fontsize='small')
plt.title("Beta drift model : "+drift_model+", hrf model : "+hrf_model)
plt.show()
"""

#save object
"""
import pickle
filepath='GLMobj/Poly10Spm'
open(filepath,"x")
with open(filepath, 'wb') as config_GLMobj:
    pickle.dump(design_matrix,config_GLMobj)
"""
 
#import object 
"""
import pickle
with open('GLMobj/CosGlv', 'rb') as config_CosGlv:
    CosGlv = pickle.load(config_CosGlv)
GLMCosGlv = mne_nirs.statistics.run_glm(BPfilt, CosGlv)
with open('GLMobj/CosGlvD', 'rb') as config_CosGlvD:
    CosGlvD = pickle.load(config_CosGlvD)
GLMCosGlvD = mne_nirs.statistics.run_glm(BPfilt, CosGlvD)
with open('GLMobj/CosGlvDD', 'rb') as config_CosGlvDD:
    CosGlvDD = pickle.load(config_CosGlvDD)
GLMCosGlvDD = mne_nirs.statistics.run_glm(BPfilt, CosGlvDD)
with open('GLMobj/CosSpm', 'rb') as config_CosSpm:
    CosSpm = pickle.load(config_CosSpm)
GLMCosSpm = mne_nirs.statistics.run_glm(BPfilt, CosSpm)
with open('GLMobj/CosSpmD', 'rb') as config_CosSpmD:
    CosSpmD = pickle.load(config_CosSpmD)
GLMCosSpmD = mne_nirs.statistics.run_glm(BPfilt, CosSpmD)
with open('GLMobj/CosSpmDD', 'rb') as config_CosSpmDD:
    CosSpmDD = pickle.load(config_CosSpmDD)
GLMCosSpmDD = mne_nirs.statistics.run_glm(BPfilt, CosSpmDD)
with open('GLMobj/Poly4Glv', 'rb') as config_Poly4Glv:
    Poly4Glv = pickle.load(config_Poly4Glv)
GLMPoly4Glv = mne_nirs.statistics.run_glm(BPfilt, Poly4Glv)
with open('GLMobj/Poly10Glv', 'rb') as config_Poly10Glv:
    Poly10Glv = pickle.load(config_Poly10Glv)
GLMPoly10Glv = mne_nirs.statistics.run_glm(BPfilt, Poly10Glv)
with open('GLMobj/Poly4Spm', 'rb') as config_Poly4Spm:
    Poly4Spm = pickle.load(config_Poly4Spm)
GLMPoly4Spm = mne_nirs.statistics.run_glm(BPfilt, Poly4Spm)
with open('GLMobj/Poly10Spm', 'rb') as config_Poly10Spm:
    Poly10Spm = pickle.load(config_Poly10Spm)
GLMPoly10Spm = mne_nirs.statistics.run_glm(BPfilt, Poly10Spm)

GLMleg=['GLMCosGlv','GLMCosGlvD','GLMCosGlvDD','GLMCosSpm','GLMCosSpmD','GLMCosSpmDD','GLMPoly4Glv ','GLMPoly10Glv ','GLMPoly4Spm ','GLMPoly10Spm ']
"""

#beta investigation
"""
import numpy
beta=numpy.zeros((40,10))
for i in range(0,40):
    beta[i,0]=GLMCosGlv.theta()[i][0]
    beta[i,1]=GLMCosGlvD.theta()[i][0]
    beta[i,2]=GLMCosGlvDD.theta()[i][0]
    beta[i,3]=GLMCosSpm.theta()[i][0]
    beta[i,4]=GLMCosSpmD.theta()[i][0]
    beta[i,5]=GLMCosSpmDD.theta()[i][0]
    beta[i,6]=GLMPoly4Glv.theta()[i][0]
    beta[i,7]=GLMPoly10Glv.theta()[i][0]
    beta[i,8]=GLMPoly4Spm.theta()[i][0]
    beta[i,9]=GLMPoly10Spm.theta()[i][0]

plt.plot(GLMCosGlv.ch_names,beta)
plt.xticks(rotation=45, ha='right',fontsize='small')
plt.legend(GLMleg, bbox_to_anchor=[1,1], loc='upper left')
plt.figure(figsize=(12,8))
plt.show()

meanbeta=numpy.mean(beta,1)   
stdbeta=numpy.var(beta,1)
plt.figure(figsize=(12,8))
plt.errorbar(GLMCosGlv.ch_names,meanbeta,yerr=stdbeta)
plt.xticks(rotation=45, ha='right',fontsize='small')
plt.show()
"""

#MSE investigation
"""
import numpy
MSE=numpy.zeros((40,10))
MSE[:,0]=GLMCosGlv.MSE()
MSE[:,1]=GLMCosGlvD.MSE()
MSE[:,2]=GLMCosGlvDD.MSE()
MSE[:,3]=GLMCosSpm.MSE()
MSE[:,4]=GLMCosSpmD.MSE()
MSE[:,5]=GLMCosSpmDD.MSE()
MSE[:,6]=GLMPoly4Glv.MSE()
MSE[:,7]=GLMPoly10Glv.MSE()
MSE[:,8]=GLMPoly4Spm.MSE()
MSE[:,9]=GLMPoly10Spm.MSE()

plt.plot(GLMCosGlv.ch_names,MSE)
plt.xticks(rotation=45, ha='right',fontsize='small')
plt.legend(GLMleg, bbox_to_anchor=[1,1], loc='upper left')
plt.figure(figsize=(12,8))
plt.show()

meanMSE=numpy.mean(MSE,1)   
stdMSE=numpy.var(MSE,1)
plt.figure(figsize=(12,8))
plt.errorbar(GLMCosGlv.ch_names,meanMSE,yerr=stdMSE)
plt.xticks(rotation=45, ha='right',fontsize='small')
plt.show()
"""