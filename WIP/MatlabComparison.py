# -*- coding: utf-8 -*-
"""
Created on Wed Mar  2 15:44:40 2022
Script to compare signal processing from Matlab and Python
@author: nojegou
"""
import scipy.io
import mne
import numpy as np
import matplotlib.pyplot as plt


Mtime = list(scipy.io.loadmat('C:/Users/nojegou/Documents/MATLAB/Script/times.mat').items())[3][1];
Mraw = list(scipy.io.loadmat('C:/Users/nojegou/Documents/MATLAB/Script/raw.mat').items())[3][1];
MOD = list(scipy.io.loadmat('C:/Users/nojegou/Documents/MATLAB/Script/OD.mat').items())[3][1];
Mconc = list(scipy.io.loadmat('C:/Users/nojegou/Documents/MATLAB/Script/conc.mat').items())[3][1];
Mfilt = list(scipy.io.loadmat('C:/Users/nojegou/Documents/MATLAB/Script/filtered.mat').items())[3][1];

Praw=mne.io.read_raw_nirx("C:/Users/nojegou/Documents/monProjetIMANES/fNIRS/gr_01/su_06/2020-07-27_001")#{},verbose=True, preload=True)
POD=mne.preprocessing.nirs.optical_density(Praw.copy())
POD._data=POD._data*1e9     #This line allow to have the same processing as MATLAB
Pconc=mne.preprocessing.nirs.beer_lambert_law(POD,ppf=6*1000) #To have the same processing as MATLAB, the PPF must be multiplied by 1000           #https://github.com/mne-tools/mne-python/pull/9843
Pfilt=Pconc.copy().filter(0.01, 0.09, l_trans_bandwidth=0.009,h_trans_bandwidth=0.01, method='fir')
"""
filt = mne.filter.create_filter(Pconc.get_data(), Pconc.info['sfreq'],method='fir', l_freq=0.01,h_freq=0.09, l_trans_bandwidth=0.009,h_trans_bandwidth=0.01)
Pfilt=Pconc
for i in range(0,40):
    Pfilt._data[i,:]= np.convolve(filt, Pconc._data[i,:])
"""
"""
Raw
"""
"""
plt.plot(Praw.times,Praw.get_data()[2,:])
plt.plot(Praw.times,Praw.get_data()[3,:])
plt.legend([Praw.ch_names[2],Praw.ch_names[3]])
plt.title("Python raw")
plt.xlabel("Time")
plt.show()

plt.plot(Mtime,Mraw[:,2])
plt.plot(Mtime,Mraw[:,3])
plt.legend([Praw.ch_names[2],Praw.ch_names[3]])
plt.title("Matlab raw")
plt.xlabel("Time")
plt.show()
"""
"""
OD
"""
"""
plt.plot(Pfilt.times,MOD[:,2])
plt.plot(Pfilt.times,MOD[:,3])
plt.legend([POD.ch_names[2],POD.ch_names[3]])
plt.title("Matlab OD")
plt.plot(POD.times[1170],MOD[1170,2], 'v')
plt.xlabel("Time")
plt.show()

plt.plot(POD.times,POD._data[2,:])
plt.plot(POD.times,POD._data[3,:])
plt.legend([POD.ch_names[2],POD.ch_names[3]])
plt.title("Python OD")
plt.plot(POD.times[1170],POD._data[2,1170], 'v')
plt.xlabel("Time")
plt.show()
"""
"""
Concentration signal 
"""
"""
plt.plot(Pfilt.times,Mconc[:,2])
plt.plot(Pfilt.times,Mconc[:,3])
plt.legend([Pfilt.ch_names[2],Pfilt.ch_names[3]])
plt.title("Matlab Concentration changes (pre-filter Data)")
plt.plot(Pfilt.times[1170],Mfilt[1170,2], 'v')
plt.xlabel("Time")
plt.show()

plt.plot(Pfilt.times,Pconc._data[2,:])
plt.plot(Pfilt.times,Pconc._data[3,:])
plt.legend([Pfilt.ch_names[2],Pfilt.ch_names[3]])
plt.title("Python Concentration changes (pre-filter Data)")
plt.plot(Pfilt.times[1170],Pconc._data[2,1170], 'v')
plt.xlabel("Time")
plt.show()
"""

"""
Concentration signal supperposition 
"""

#ScaleCoeffConc=60010424 #First coefficient :41367602.731131814
plt.plot(Pfilt.times,Mconc[:,2])
plt.plot(Pfilt.times,Mconc[:,3])
plt.plot(Pfilt.times,Pconc._data[2,:])#*60010424.759372056)
plt.plot(Pfilt.times,Pconc._data[3,:])#*60010424.75940124)
plt.plot(Pfilt.times[1170],Mconc[1170,2], 'v')
plt.plot(Pfilt.times[1170],Pconc._data[2,1170], 'v')#*60010424.759372056
plt.plot(Pfilt.times[4740],Mconc[4740,2], 'o')
plt.plot(Pfilt.times[4740],Pconc._data[2,4740], 'o')#*60010424.759372056
plt.legend([str("Matlab "+Pfilt.ch_names[2]),str("Matlab "+Pconc.ch_names[3]),str("Python "+Pfilt.ch_names[2]),str("Python "+Pfilt.ch_names[3]),"Reference points"])
plt.title("Matlab and Python Concentration changes (pre-filter Data)")
plt.xlabel("Time")
plt.show()


"""
Filtered signal plot
"""
"""
plt.plot(Pfilt.times,Mfilt[:,2])
plt.plot(Pfilt.times,Mfilt[:,3])
plt.legend([Pfilt.ch_names[2],Pfilt.ch_names[3]])
plt.title("Matlab filtered signal")
plt.plot(Pfilt.times[1170],Mfilt[1170,2], 'v')
plt.show()
ScaleCoeff=53926979
plt.plot(Pfilt.times,Pfilt._data[2,:]*ScaleCoeff)
plt.plot(Pfilt.times,Pfilt._data[3,:]*ScaleCoeff)
plt.legend([Pfilt.ch_names[2],Pfilt.ch_names[3]])
plt.title("Python filtered signal")
plt.plot(Pfilt.times[1170],Pfilt._data[2,1170]*ScaleCoeff, 'v')
plt.xlabel("Time")
plt.show()
"""

"""
Filtered signal superposition
"""
#ScaleCoeff=60010424#59110202    #First Coefficient : 53930000
#Pfilt._data=Pfilt._data*ScaleCoeff

plt.plot(Pconc.times,Mfilt[:,2])
plt.plot(Pconc.times,Mfilt[:,3])
plt.legend()
plt.title("Matlab and Python filtered signal")

plt.plot(Pconc.times,Pfilt._data[2,:])
plt.plot(Pconc.times,Pfilt._data[3,:])
plt.plot(Pconc.times[1170],Mfilt[1170,2], 'v')
plt.plot(Pconc.times[1170],Pfilt._data[2,1170], 'v')
plt.plot(Pconc.times[4740],Mfilt[4740,2], 'o')
plt.plot(Pconc.times[4740],Pfilt._data[2,4740], 'o')
plt.legend([str("Matlab "+Pfilt.ch_names[2]),str("Matlab "+Pfilt.ch_names[3]),str("Python "+Pfilt.ch_names[2]),str("Python "+Pfilt.ch_names[3]),"Reference points"])
plt.xlabel("Time")
plt.show()



"""
Time signal comparison
"""
"""
plt.scatter(np.arange(0,len(Mtime)),Mfilt)
plt.scatter(np.arange(0,len(Mtime)),Praw.times)
plt.xlim(3000,3020)
plt.ylim(382,388)
plt.show()
"""
"""
Time, Raw, Concentration changes and Filtered absolute difference
"""
difftime=abs(Mtime[:,0]-np.transpose(Praw.times[:]))
diffraw=abs(Mraw-np.transpose(Praw.get_data()))
diffconc=abs(Mconc-np.transpose(Pconc.get_data()))
difffilt=abs(Mfilt-np.transpose(Pfilt.get_data()))
# for i in range(1,5000):
#     if(difftime[i] !=difftime[i-1]) : print(difftime[i])
"""
plt.scatter(np.arange(0,len(difftime)),difftime)
plt.xlim(0,len(difftime))
plt.ylim(0.127999999999921,0.128000000000049)
plt.title("Difference between Matlab and Python times (Scale is between 0.127999999999921 and 0.128000000000049)")
plt.xlabel("Time")
plt.show()

rawnull=0
for i in range(0,len(diffraw[1,:])):
    for j in range(0,len(diffraw)):
        if(diffraw[j,i]!=0): 
            print("raw diff de 0")
            rawnull=rawnull+1;
if(rawnull==0):
    print("Aucune différence entre le signal raw Matlab et python")     
"""              
#     if(difftime[i] !=difftime[i-1]) : print(difftime[i])
# for i in range(0,40):
#     plt.scatter(np.arange(0,len(diffraw)),diffraw[:,i])
"""
for i in range(0,len(diffraw[1,:])):
    plt.plot(np.arange(0,len(diffraw)),diffconc[:,i])
    plt.title("Différence entre les concentration calculés sur Matlab et Python")
plt.legend(Praw.ch_names,bbox_to_anchor=[1,1], loc='upper left')
plt.xlim(0,len(diffraw))
plt.show()
 
for i in range(0,len(diffraw[1,:])):
    plt.plot(np.arange(0,len(diffraw)),difffilt[:,i])
    plt.title("Différence entre les signaux filtrés calculés sur Matlab et Python")
plt.legend(Praw.ch_names,bbox_to_anchor=[1,1], loc='upper left')
plt.xlim(0,len(diffraw))
plt.show()
"""
"""
Mean absolute difference
"""
plt.plot(np.mean(diffraw,1))
plt.plot(np.mean(diffconc[:,0:40:2],1))
plt.plot(np.mean(diffconc[:,1:40:2],1))
plt.plot(np.mean(difffilt[:,0:40:2],1))
plt.plot(np.mean(difffilt[:,1:40:2],1))
plt.xlim(0,len(diffraw))
plt.legend(["Moyenne de la différence absolue des signaux entrant","Moyenne de la différence absolue des signaux de concentration HbR","Moyenne de la différence absolue des signaux de concentration HbO","Moyenne de la différence absolue des HbR filtrés","Moyenne de la différence absolue des HbO filtrés"])
plt.show()

"""
Frequency absolute difference
"""
"""
Legend=[]
for i in range(1,len(diffraw[1,:]),2):
    plt.psd(Pconc._data[i],NFFT=120,noverlap=60,Fs=Praw.info['sfreq'],linestyle='--')
    plt.psd(Mconc[:,i],NFFT=120,noverlap=60,Fs=Praw.info['sfreq'])
    Legend.append("Python "+Praw.ch_names[i])
    Legend.append("Matlab "+Praw.ch_names[i])
plt.legend(Legend,bbox_to_anchor=[1,1], loc='upper left')
plt.ylim(-160,60)
plt.xlim(0,4)
plt.title("PSD de la varaition de concentration HbO Matlab et Python")
plt.show()
    
Legend=[]
for i in range(0,len(diffraw[1,:]),2):
    plt.psd(Pconc._data[i],NFFT=120,noverlap=60,Fs=Praw.info['sfreq'],linestyle='--')
    plt.psd(Mconc[:,i],NFFT=120,noverlap=60,Fs=Praw.info['sfreq'])
    Legend.append("Python "+Praw.ch_names[i])
    Legend.append("Matlab "+Praw.ch_names[i])
plt.legend(Legend,bbox_to_anchor=[1,1], loc='upper left')
plt.ylim(-160,60)
plt.xlim(0,4)
plt.title("PSD de la varaition de concentration HbR Matlab et Python")
plt.show()    
"""  
"""
Correlation
"""
ConcCorr=np.zeros([40])
for i in range(0,len(diffraw[1,:])):
    ConcCorr[i]=np.mean(np.corrcoef(np.transpose(Mconc)[:,i],Pconc.get_data()[:,i],rowvar=True))
plt.plot(Pconc.ch_names,ConcCorr)
plt.xlabel("Channels")
plt.xlim([0,39])
plt.ylim([0,1.1])
plt.title("Normalized correlation of concentration changes per channel")
plt.show()

ConcCorr=np.zeros([40])
for i in range(0,len(diffraw[1,:])):
    ConcCorr[i]=np.mean(np.corrcoef(np.transpose(Mfilt)[:,i],Pfilt.get_data()[:,i],rowvar=True))
plt.plot(Pfilt.ch_names,ConcCorr)
plt.xlabel("Channels")
plt.xlim([0,39])
plt.ylim([0,1.1])
plt.title("Normalized correlation of filtered signal per channel")
plt.show()

"""
Scatter #doesn't work because the difference of concentration are very different
"""
# for i in range (0,40):
#     plt.scatter(Pfilt.times,Pfilt._data[i,:],color='b')
#     plt.scatter(Pfilt.times,Mfilt[:,i],color='r')
# plt.show()