import os
import mne
import matplotlib.pyplot as plt
from mne.preprocessing.nirs import (optical_density, temporal_derivative_distribution_repair)
"""
fnirs_data_folder = mne.datasets.fnirs_motor.data_path()
fnirs_cw_amplitude_dir = os.path.join(fnirs_data_folder, 'Participant-1')
raw_intensity = mne.io.read_raw_nirx(fnirs_cw_amplitude_dir, verbose=True)
raw_intensity.load_data().resample(3, npad="auto")
raw_od = optical_density(raw_intensity)
new_annotations = mne.Annotations([31, 187, 317], [8, 8, 8],
                                  ["Movement", "Movement", "Movement"])
raw_od.set_annotations(new_annotations)
raw_od.plot(n_channels=15,title="Raw signal", duration=400, show_scrollbars=False)
plt.show()
corrupted_data = raw_od.get_data()
corrupted_data[:, 298:302] = corrupted_data[:, 298:302] - 0.06
corrupted_data[:, 450:750] = corrupted_data[:, 450:750] + 0.03
corrupted_od = mne.io.RawArray(corrupted_data, raw_od.info,  first_samp=raw_od.first_samp)
new_annotations.append([95, 145, 245], [10, 10, 10], ["Spike", "Baseline", "Baseline"])
corrupted_od.set_annotations(new_annotations)

corrupted_od.plot(n_channels=15,title="Corrupted data", duration=400, show_scrollbars=False)
plt.show()

corrected_tddr = temporal_derivative_distribution_repair(corrupted_od)
corrected_tddr.plot(n_channels=15, duration=400, show_scrollbars=False)
plt.show()
"""
#-----------------------------------------------------------------------------------------------------------------------
"""
sample_data_folder = mne.datasets.sample.data_path()
sample_data_raw_file = os.path.join(sample_data_folder, 'MEG', 'sample',
                                    'sample_audvis_filt-0-40_raw.fif')
raw = mne.io.read_raw_fif(sample_data_raw_file)
raw.crop(tmax=60.).pick_types(meg='mag', eeg=True, stim=True, eog=True)
raw.load_data()
raw.plot(n_channels=15,title="ICA filtered corrupted", duration=400, show_scrollbars=False)
filt_raw = raw.copy().filter(l_freq=1., h_freq=None)#(0.01, 0.09, l_trans_bandwidth=0.009,h_trans_bandwidth=0.01, method='fir')
filt_raw.plot(n_channels=15,title="Filtered corrupted", duration=400, show_scrollbars=False)
ica = mne.preprocessing.ICA(n_components=15, max_iter='auto', random_state=97)
ica.fit(filt_raw)
ica.exclude = [0, 1]
reconstruc_raw=raw.copy()
ica.apply(reconstruc_raw)
raw.plot(n_channels=15, show_scrollbars=False)
reconstruc_raw.plot(n_channels=15, show_scrollbars=False)
"""


    