# -*- coding: utf-8 -*-
"""
Created on Wed Jan 19 11:05:54 2022
Study of the impact of PPF
@author: nojegou
"""
import mne
import matplotlib.pyplot as plt
#import numpy as np

###Import data
raw_data=mne.io.read_raw_nirx("C:/Users/nojegou/Documents/monProjetIMANES/fNIRS/gr_01/su_06/2020-07-27_001",verbose=True, preload=True)
tstart = raw_data.annotations.onset[1]
#raw_data.plot(n_channels=len(raw_data.ch_names), duration=200, show_scrollbars=True, scalings='auto', clipping=None, start=tstart)
OD=mne.preprocessing.nirs.optical_density(raw_data.copy())
"""
Conc=mne.preprocessing.nirs.beer_lambert_law(OD,ppf=6)

i=0
while i <40:#len(raw_data.ch_names)/2):
    plt.psd(raw_data._data[i], Fs=7.812500)  #Sampling rate ???
    i+=1
plt.xlim(0.15,3.8)
plt.title("PSD raw")
plt.show()
i=0
while i <20:#len(raw_data.ch_names)/2):
    plt.psd(Conc._data[i], Fs=7.812500)  #Sampling rate ???
    i+=1
plt.xlim(0.15,3.8)
plt.title("PSD Concentration")
plt.legend(Conc.ch_names[0:20])
plt.show()

#Conc.plot(n_channels=len(raw_data.ch_names), duration=200, show_scrollbars=True, scalings='auto', clipping=None, start=tstart)

###Lowpass filter
LPfilt=Conc.copy().filter(None, 0.1)
#.plot(n_channels=len(LPfilt.ch_names), duration=200, show_scrollbars=True, scalings='auto', clipping=None, start=tstart)

###Bandpass filter
BPfilt=Conc.copy().filter(0.01, 0.1)
#LPfilt.plot(n_channels=len(LPfilt.ch_names), duration=200, show_scrollbars=True, scalings='auto', clipping=None, start=tstart)

i=0
while i <20:#len(raw_data.ch_names)/2):
    plt.psd(BPfilt._data[i],Fs=7.812500)  #Sampling rate ???
    plt.psd(LPfilt._data[i],Fs=7.812500)
    i+=1
plt.xlim(0.15,3.8)
plt.title("PSD Bandpass & Lowpass")
plt.show()
"""
Conc6=mne.preprocessing.nirs.beer_lambert_law(OD,ppf=5)
Conc7=mne.preprocessing.nirs.beer_lambert_law(OD,ppf=7)

Filt6=Conc6.copy().filter(0.01, 0.09, l_trans_bandwidth=0.009,h_trans_bandwidth=0.01, method='fir')
Filt7=Conc7.copy().filter(0.01, 0.09, l_trans_bandwidth=0.009,h_trans_bandwidth=0.01, method='fir')
"""
plt.plot(Conc6.times,Conc6._data[2,:])
plt.plot(Conc6.times,Conc6._data[3,:])
plt.plot(Conc6.times,Conc7._data[2,:])#*60010424.759372056)
plt.plot(Conc6.times,Conc7._data[3,:])#*60010424.75940124)
plt.legend([str("PPF=6 "+Conc6.ch_names[2]),str("PPF=6 "+Conc6.ch_names[3]),str("PPF=7 "+Conc6.ch_names[2]),str("PPF=7 "+Conc6.ch_names[3])])
plt.title("Matlab and Python Concentration changes (pre-filter Data)")
plt.xlabel("Time")
plt.show()
"""
plt.plot(Conc6.times,Filt6._data[2,:])
plt.plot(Conc6.times,Filt6._data[3,:])
plt.plot(Conc6.times,Filt7._data[2,:])
plt.plot(Conc6.times,Filt7._data[3,:])
plt.legend([str("PPF=5 "+Conc6.ch_names[2]),str("PPF=5 "+Conc6.ch_names[3]),str("PPF=7 "+Conc6.ch_names[2]),str("PPF=7 "+Conc6.ch_names[3])])
plt.title("Matlab and Python Concentration changes (pre-filter Data)")
plt.xlabel("Time")
plt.show()