# -*- coding: utf-8 -*-
"""
Created on Wed Jul  6 11:44:04 2022

@author: nojegou
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Jul  6 09:46:44 2022

@author: nojegou
"""

import mne
import mne_nirs
import mne_bids
import pandas as pd
import matplotlib.pyplot as plt


def individual_analysis(bids_path, ID):
    """
    ID="062"
    BidsPath="C:/Users/nojegou/Documents/Data/IMANES-BIDS"
    #subjects=["055", "056", "061", "062", "075", "076", "081", "082", "083", "091", "092", "106", "107", "111", "112"]
    subjects="055"
    dataset=mne_bids.BIDSPath(root=BidsPath, subject = subjects, task="motortask", datatype="nirs", suffix="nirs", extension=".snirf")
    """
    raw_data = mne_bids.read_raw_bids(bids_path=dataset, extra_params=None, verbose=True)
    #raw_data.annotations.delete(raw_data.annotations.description=="Intro")
    #raw_data.annotations.delete(raw_data.annotations.description=="Restingstate")
    #raw_data.crop(raw_data.annotations.onset[1]+1)
    #delete unused signal
    i=0
    RightTrig=False
    while ((i<len(raw_data.annotations.description)) and not(RightTrig)):
        RightTrig=(raw_data.annotations.description[i]=='Motortask')
        if (RightTrig): raw_data.crop(raw_data.annotations.onset[i]-27,raw_data.annotations.onset[i]+182)
        else : i=i+1
    raw_data.annotations.delete(0)
    # Convert signal to haemoglobin and resample
    OD = mne.preprocessing.nirs.optical_density(raw_data)
    Conc = mne.preprocessing.nirs.beer_lambert_law(OD,ppf=6)
    #Conc.resample(0.6, verbose=True)
    BPfilt = Conc.copy().filter(0.01, 0.09, l_trans_bandwidth=0.01,h_trans_bandwidth=0.01, method='fir')#BPfilt = Conc.copy().filter(0.01, None, l_trans_bandwidth=0.01, method='fir')
    
    #Delete irrelevent triggers
    """
    i=0
    RightTrig=False
    while ((i<len(BPfilt.annotations.description)) and not(RightTrig)):
        RightTrig=(BPfilt.annotations.description[i]=='Motortask')
        if (RightTrig): BPfilt.crop(BPfilt.annotations.onset[i]-50,BPfilt.annotations.onset[i+3]+45)
        
        else : BPfilt.annotations.delete([i])
    triggerLen = len(BPfilt.annotations.description)
    if (triggerLen > 4): BPfilt.annotations.delete([-(triggerLen - 4)])
"""
    events, event_dict = mne.events_from_annotations(BPfilt.copy(), verbose=False)
    epochs = mne.Epochs(BPfilt.copy(), events, event_id=event_dict, tmin=-5,tmax=45,preload=True)#mne.Epochs(BPfilt.copy().pick(['S3_D3 hbr','S3_D3 hbo','S6_D7 hbr','S6_D7 hbo']), events, event_id=event_dict, tmin=-10,tmax=45,preload=True)
    return epochs

#___________________________________________________________________________________________________________________________________________________________________________
# Group Analysis

epochlist=[]

BidsPath="C:/Users/nojegou/Documents/Data/IMANES-BIDS-both-session"
dataset = mne_bids.BIDSPath(session="2", root=BidsPath, task="motortask", datatype="nirs", suffix="nirs", extension=".snirf")
subjects = mne_bids.get_entity_vals(BidsPath, 'subject') #['01','04','05','06','07','08','09','10','11']
for sub in subjects:
    bids_path = dataset.update(subject=sub)
    epochs=individual_analysis(bids_path,sub)
    print()
    epochlist.append(epochs)
    Epochs=mne.concatenate_epochs(epochlist)
    

#plot evoked topomap
from collections import defaultdict
for Sujet in range(0,9):
    all_evokeds = defaultdict(list)
    plt.ion()
    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(6, 4), dpi=150)
    mne.viz.plot_evoked_topo(epochlist[Sujet]['Motortask'].average(picks='hbo'), color='r',
                         axes=axes, layout_scale=1 ,legend=False, show=False, ylim=dict(hbo=[-0.1, 0.6]), title=str("Sub "+subjects[Sujet]+" - Sess "+dataset.session))
    mne.viz.plot_evoked_topo(epochlist[Sujet]['Motortask'].average(picks='hbr'), color='b',
                         axes=axes, layout_scale=1, legend=False, show=False, ylim=dict(hbr=[-0.1, 0.6]))
    leg_lines = [line for line in axes.lines if line.get_c() == 'r'][:1]
    leg_lines.append([line for line in axes.lines if line.get_c() == 'b'][0])
    fig.legend(leg_lines, ['HbO', 'HbR'], loc='lower right')
    #axes.title(str("Sub "+str(Sujet)+" - Sess "+Session))
    fig.show()



#to see specific events from sprcific patient and channel 
#epochs['Motortask'].pick(['S1_D1 hbo']).plot_image()

"""
    for cidx, condition in enumerate(epochs.event_id):
        all_evokeds[condition].append(epochs[condition].average())
import matplotlib.pyplot as plt
    
fig, axes = plt.subplots(nrows=1, ncols=len(all_evokeds), figsize=(17, 17),squeeze=False)
lims = dict(hbo=[-5, 12], hbr=[-5, 12])

for (pick, color) in zip(['hbo', 'hbr'], ['r', 'b']):
    print("hey")
    for idx, evoked in enumerate(all_evokeds):
        mne.viz.plot_compare_evokeds({evoked: all_evokeds[evoked]}, combine='mean',
                             picks=pick, axes=axes[idx], show=False,
                             colors=[color], legend=False, ylim=lims, ci=0.95,
                             show_sensors=idx == 2)
        axes[idx].set_title('{}'.format(evoked))
axes[0].legend(["Oxyhaemoglobin", "Deoxyhaemoglobin"])
"""