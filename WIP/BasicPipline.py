# -*- coding: utf-8 -*-
"""
Created on Wed Jan 19 11:05:54 2022
This script takes (l 14) Raw data -> (l 15) Optical density -> (l 16)Concentration changes (ppf=6) -> (l17)Bandpass FIR filter ([0.01 0.09] with a 0.01 transition band)
@author: nojegou
"""

import mne
import matplotlib.pyplot as plt

###Import data
#_______________________________________________________________________________
raw_data=mne.io.read_raw_nirx("C:/Users/nojegou/Documents/monProjetIMANES/fNIRS/gr_01/su_06/2020-07-27_001",verbose=True, preload=True)   #Indicate where the directory of your data is
OD=mne.preprocessing.nirs.optical_density(raw_data.copy()) # convert Raw data to optical density
Conc=mne.preprocessing.nirs.beer_lambert_law(OD,ppf=6)  # convert to Concentration changes
BPfilt=Conc.copy().filter(0.01, 0.09, l_trans_bandwidth=0.009,h_trans_bandwidth=0.01, method='fir')   #filtering concentration changes to remove unwanted informations
"""
#If you want the same concentration changes values as Matlab you use the following lines insted of line 14
OD._data=OD._data*1e9                                                #This line allow to have the same processing as MATLAB
Conc=mne.preprocessing.nirs.beer_lambert_law(OD,ppf=6*1000)          #This line allow to have the same processing as MATLAB
#If th lines 31,32,33 and 44,45,46 are need to be changed to asses the nex values of signals
"""
###Plot raw data
#_______________________________________________________________________________
#raw_data.plot(n_channels=len(raw_data.ch_names), duration=200, show_scrollbars=True, scalings='auto', clipping=None, start=BPfilt.annotations.onset[1])

###Plot Concentration Changes of specific channel
#_______________________________________________________________________________
'''
ch_number=0         #chose an even value from 0 to 40
plt.plot(BPfilt.times,Conc._data[ch_number])           #Plot the HbO
plt.plot(BPfilt.times,Conc._data[ch_number+1])         #Plot the HbR
plt.vlines([Conc.annotations.onset[0]], 1.5e-6,-1.5e-6 ,colors="gold")            #Plots the resting state trigger in yellow
plt.vlines([Conc.annotations.onset[1]], 1.5e-6,-1.5e-6 ,colors="royalblue")       #Plots the introduction trigger in blue
plt.vlines([Conc.annotations.onset[2:6]], 1.5e-6,-1.5e-6 ,colors="forestgreen")   #Plots the motor task trigger in green
plt.legend(["HbO Concentration changes","HbR Concentration changes"])
plt.title(Conc.ch_names[ch_number]+" & "+Conc.ch_names[ch_number+1]+" channel Concentration changes",fontsize=30,  fontweight="bold")
plt.xlim(BPfilt.annotations.onset[1]-5,750)
plt.show()
 '''
###Plot Filtered data of specific channel
#_______________________________________________________________________________
ch_number=14         #chose an even value from 0 to 40
plt.plot(BPfilt.times,BPfilt._data[ch_number])           #Plot the HbO
plt.plot(BPfilt.times,BPfilt._data[ch_number+1])         #Plot the HbR
plt.vlines([BPfilt.annotations.onset[0]], 1e-6,-1.5e-6 ,colors="gold")            #Plots the resting state trigger in yellow
plt.vlines([BPfilt.annotations.onset[1]], 1e-6,-1.5e-6 ,colors="royalblue")       #Plots the introduction trigger in blue
plt.vlines([BPfilt.annotations.onset[2:6]], 1e-6,-1.5e-6 ,colors="forestgreen")   #Plots the motor task trigger in green
plt.legend(["HbO Bandpass signal","HbR Bandpass signal"])
plt.title(BPfilt.ch_names[ch_number]+" & "+BPfilt.ch_names[ch_number+1]+" channel with a Bandpass filter",  fontweight="bold")
plt.xlim(BPfilt.annotations.onset[1]-5,750)
plt.show()

###Plot PSD of Raw data 
#_______________________________________________________________________________
'''
i=0
while i <40:#len(raw_data.ch_names)/2):
    plt.psd(raw_data._data[i], Fs=7.812500)  #Sampling rate ???
    i+=2
plt.xlim(0.15,3.8)
plt.title("PSD raw of 785 signals")
plt.legend(raw_data.ch_names[0:40:2],bbox_to_anchor=[1,1],loc='upper left')
plt.show()
i=1
while i <40:#len(raw_data.ch_names)/2):
    plt.psd(raw_data._data[i], Fs=7.812500)  #Sampling rate ???
    i+=2
plt.xlim(0.15,3.8)
plt.title("PSD raw of 830 signals")
plt.legend(raw_data.ch_names[0:40:2],bbox_to_anchor=[1,1],loc='upper left')
plt.show()
'''
###Plot PSD of Concentration changes
#_______________________________________________________________________________
'''
i=0
while i <40:#len(raw_data.ch_names)/2):
    plt.psd(Conc._data[i], Fs=7.812500)  #Sampling rate ???
    i+=2
plt.xlim(0.15,3.8)
plt.title("PSD Concentration of HbO")
plt.legend(Conc.ch_names[0:40:2],bbox_to_anchor=[1,1],loc='upper left')
plt.show()
i=1
while i <40:#len(raw_data.ch_names)/2):
    plt.psd(Conc._data[i], Fs=7.812500)  #Sampling rate ???
    i+=2
plt.xlim(0.15,3.8)
plt.title("PSD Concentration of HbR")
plt.legend(Conc.ch_names[0:40:2],bbox_to_anchor=[1,1],loc='upper left')
plt.show()
'''
###Plot PSD of Filterd data
#_______________________________________________________________________________
'''
i=0
while i <40:#len(raw_data.ch_names)/2):
    plt.psd(BPfilt._data[i], Fs=7.812500)  #Sampling rate ???
    i+=2
plt.xlim(0.15,3.8)
plt.title("PSD Filtered data HbO")
plt.legend(BPfilt.ch_names[0:40:2],bbox_to_anchor=[1,1],loc='upper left')
plt.show()
i=1
while i <40:#len(raw_data.ch_names)/2):
    plt.psd(BPfilt._data[i], Fs=7.812500)  #Sampling rate ???
    i+=2
plt.xlim(0.15,3.8)
plt.title("PSD Filtered data HbR")
plt.legend(BPfilt.ch_names[0:40:2],bbox_to_anchor=[1,1],loc='upper left')
plt.show()
'''
