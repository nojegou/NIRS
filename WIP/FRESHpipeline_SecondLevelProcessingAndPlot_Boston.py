# -*- coding: utf-8 -*-
"""
Created on Tue Apr 25 16:25:58 2023

# Authors: Nolwenn Jegou <nolwenn.jegou@inria.fr>
"""
import mne 
import mne_nirs
import pandas as pd
import mne_bids
import argparse
from textwrap import dedent
import statsmodels.formula.api as smf
from pickle import load
import numpy as np
import datetime
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import os


def plot_2nd_level_res(fig, axes, model_df, coordinates, genset_bad_channels):
    """
    plot and save the second level GLM of data. Each point is the center of each channel. A channel as marked as un

    Parameters
    ----------
    fig : matplotlib.figure.Figure
        DESCRIPTION.
    axes : Array of MatplotlTYPE Axes
        Axe along with every GLM result wil be plot.
    model_df : Dataframe
        Result of second level analysis.
    coordinates : mne.channels.montage.DigMontage
        Coordinates of channels.
    genset_bad_channels : Set
        Set of bad channels included in all subjects.

    Returns
    -------
    None.

    """

    fig.suptitle("Second level GLM",fontsize='xx-large',weight='bold')
    plt.tight_layout(pad=2)
    
    #legend definition
    redot=mpl.lines.Line2D([], [], color='r', marker='o', linestyle='None', markersize=10, label='Activated channel', alpha=1)
    blackdot=mpl.lines.Line2D([], [], color='k', marker='o', linestyle='None', markersize=8, label='Not activated channel', alpha=1)
    circle=mpl.lines.Line2D([], [], marker='o', linestyle='None', markersize=8, markerfacecolor='none', markeredgecolor='k', label='Did not pass quality test', alpha=1)
     
     
    for i,cond in enumerate(set(model_df.Condition)): #plot of every condition, in an individual subplot
        axes[i].set_title(cond,fontsize='x-large') #subtitle
        axes[i].set_xticks([])
        axes[i].set_yticks([])
        axes[i].legend(handles=[redot, blackdot,circle], loc="lower center", bbox_to_anchor=(0.5,-0.10)) #legend
        axis_model_DF_hbo=model_df[(model_df["Condition"]==cond) & (model_df["ch_name"].str[-1]=='o')]
        axis_model_DF_hbr=model_df[(model_df["Condition"]==cond) & (model_df["ch_name"].str[-1]=='r')]
        
        
        #display right colors over each channels
        for chname in axis_model_DF_hbo["ch_name"]:
            chcoord=coordinates._get_ch_pos()[chname]
            hboline=model_df[(model_df["Condition"]==cond) & (model_df["ch_name"]==chname)] #get hbo line from model_df corresponding to current condition and channel name
            hbrline=model_df[(model_df["Condition"]==cond) & (model_df["ch_name"]==chname[:-1]+'r')]
            
            
            #Channel doesn't pass quality test
            if chname[0:-4] in genset_bad_channels :              
                axes[i].scatter(chcoord[0],chcoord[1], s=100, facecolors='none', edgecolors='k')
            
            
            #Activates channels
            elif ((hboline["Coef."].values>0 and hbrline["Coef."].values<0) and (hboline["fdr_p_value"].values<0.05 and hbrline["fdr_p_value"].values<0.05)):
                axes[i].scatter(chcoord[0],chcoord[1], s=200, color='r')
           
            
           #Not activated channels 
            else:
                axes[i].scatter(chcoord[0],chcoord[1], s=100, color='k')
   
    
    plt.show()
    
def main():
    

    #gets data from data path to get number of sessions, channels coordinates and annotations
    dataset = mne_bids.BIDSPath(root=pipeline.get_bids_path(), task=mne_bids.get_entity_vals(pipeline.get_bids_path(), 'task')[0], datatype="nirs", suffix="nirs", extension=".snirf")
    subjects = mne_bids.get_entity_vals(pipeline.get_bids_path(), 'subject')
    sessions=mne_bids.get_entity_vals(pipeline.get_bids_path(),'session')
    if (sessions==[]): sessions=[None]
    bids_path=dataset.update(subject=subjects[0], session=sessions[0])
    raw_data = mne_bids.read_raw_bids(bids_path=bids_path, extra_params=None, verbose=True)
    coordinates = mne.preprocessing.nirs.beer_lambert_law(mne.preprocessing.nirs.optical_density(raw_data)).get_montage() #coordinates to plot result at the right space
    annotations=set(raw_data.annotations.description) 
    del raw_data


    # open bad channels and put them in a set
    general_bad_channels=list()
    genset_bad_channels=set()
    for sub in subjects:
        with open(pipeline.get_bids_path()+'/derivatives/FRESH-pipeline/sub_'+sub+'_bad_channels.pkl', 'rb') as f:
            general_bad_channels.append(load(f))            
    for idx in range(len(subjects)):
        for j in range(len(general_bad_channels[idx][0])):
            genset_bad_channels.add(general_bad_channels[idx][0][j][:5])
                 

    #download GLM results and put it in a big panda dataframe
    group_glm_DF=pd.DataFrame()
    for sub in subjects:
        group_glm_DF=pd.concat([group_glm_DF, pd.read_pickle(pipeline.get_bids_path()+"/derivatives/FRESH-pipeline/sub_"+sub+"_glmDF.pkl")], ignore_index=True)


    #List names of conditions
    conditionsName=list()
    for cond in set(group_glm_DF.Condition):
        for anno in annotations:
            if anno in cond : conditionsName.append(cond)


    #second level GLM    
    group_res = group_glm_DF.query("Condition in "+str(conditionsName)).copy(deep=True) ## delete groupGLMDF afterward
    group_res["theta"]=[t*1.e6 for t in group_res["theta"]] # Convert to uM for nicer plotting below
    formule="theta ~ -1 + ch_name:Condition"
    model = smf.mixedlm(formule, group_res, groups=group_res["ID"]).fit(method='nm')   #https://www.statsmodels.org/stable/generated/statsmodels.formula.api.mixedlm.html#statsmodels.formula.api.mixedlm
    
       
    # saving second level glm dataframe
    model_df = mne_nirs.statistics.statsmodels_to_results(model)
    
    #FDR corrected p_values added to DF
    columns_names=model_df.keys().to_list()
    columns_names.append('fdr_p_value')
    final_DF=pd.DataFrame(columns=columns_names)
    for cond in set(model_df.Condition):
        model_DF_hbo=model_df[(model_df["Condition"]==cond) & (model_df["ch_name"].str[-1]=='o')]
        model_DF_hbr=model_df[(model_df["Condition"]==cond) & (model_df["ch_name"].str[-1]=='r')]
        rejectedhbo, corr_p_val_hbo=mne.stats.fdr_correction(model_DF_hbo["P>|z|"])
        rejectedhbr, corr_p_val_hbr=mne.stats.fdr_correction(model_DF_hbr["P>|z|"])
        model_DF_hbo.insert(9,'fdr_p_value',corr_p_val_hbo)
        model_DF_hbr.insert(9,'fdr_p_value',corr_p_val_hbr)
        final_DF=pd.concat([model_DF_hbo,model_DF_hbr,final_DF])
    
    #save Dataframe
    final_DF.to_pickle(pipeline.get_bids_path()+"/derivatives/FRESH-pipeline/SecondLevel_glmDF.pkl") 
    if(pipeline.get_excel()): final_DF.to_excel(pipeline.get_bids_path()+"/derivatives/FRESH-pipeline/SecondLevel_glmDF.xlsx")  ###################pb to look into


    #create figure
    fig, axes = plt.subplots(nrows=1, ncols=len(conditionsName), figsize=(len(conditionsName)*7,7))#,gridspec_kw=dict(width_ratios=[4, 1]))
     

    plot_2nd_level_res(fig, axes, final_DF, coordinates, genset_bad_channels)
         
         
    fig.savefig(pipeline.get_bids_path()+'/derivatives/FRESH-pipeline/Second_Level_Analysis.png')


    #Save pipeline parameters
    parameters = open(pipeline.get_bids_path()+"/derivatives/FRESH-pipeline/parmetersFile.txt","a")
    parameters.write("\nGroup level analysis parameters.\n________________\nDatapath = "+pipeline.get_bids_path()+"\nDate and time = "+str(datetime.datetime.now())+"\nFormule = "+formule)
    parameters.close()
       
        
if __name__ == "__main__":
    
    
    #options for command line running
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description=dedent('''Second level analysis of a dataset. Results will be stores in BidsPath/derivatives/FRESH-pipeline. 
    Must be run after FRESHpipeline-GroupProcessing.py and FERSHpipeline-FirstLevelAnalysis.py'''))
    parser.add_argument('BidsPath', type=str, help='Path to DataFolder in bids format - for Windows "\" must be changes to "/"')
    parser.add_argument('--PlotFigure','-p', type=bool, default=False, help='Display figure for each subject')
    args = parser.parse_args()
    display = args.PlotFigure
    bids_path = args.BidsPath
    
    
    os.chdir("..")
    from FRESHpipeline_FirstLevelProcessing import Pipeline  
    with open(bids_path+'/derivatives/FRESH-pipeline/pipeline.pkl', 'rb') as file:
        pipeline = load(file)
    
        
    main() 