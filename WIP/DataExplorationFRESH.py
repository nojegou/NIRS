# -*- coding: utf-8 -*-
"""
Created on Mon Jan 30 11:17:51 2023

@author: nojegou
"""
import pandas as pd
import mne
import mne_nirs
import mne_bids
import pickle as pkl
import matplotlib.pyplot as plt
import numpy as np
import matplotlib as mpl
from pickle import load

BidsPath="C:/Users/nojegou/Documents/Data/IMANES-BIDS-both-session"
dataset=mne_bids.BIDSPath(root=BidsPath, task=mne_bids.get_entity_vals(BidsPath, 'task')[0], datatype="nirs", suffix="nirs", extension=".snirf")
subjects = mne_bids.get_entity_vals(BidsPath, 'subject')
#session=mne_bids.get_entity_vals(BidsPath,'session')

#import dataframe created in Group analysis SSD
glm_est_DF=list()
bad_channels=list()
for idx, sub in enumerate(subjects):
    glm_est_DF.append(pd.read_pickle("C:/Users/nojegou/Documents/Data/IMANES-BIDS-both-session/derivatives/FRESH-pipeline/sub_"+sub+"_glmDF.pkl"))
    with open('C:/Users/nojegou/Documents/Data/IMANES-BIDS-both-session/derivatives/FRESH-pipeline/sub_'+sub+'_bad_channels.pkl', 'rb') as f:
        bad_channels.append(load(f))


bids_path=dataset.update(subject=sub,session='1')
raw_data = mne_bids.read_raw_bids(bids_path=dataset, extra_params=None, verbose=True)
coordinates = mne.preprocessing.nirs.beer_lambert_law(mne.preprocessing.nirs.optical_density(raw_data)).get_montage()

#motor_ch=['S3_D2 hbo','S3_D8 hbo','S3_D9 hbo','S5_D13 hbo','S5_D16 hbo','S10_D24 hbo','S10_D18 hbo','S10_D25 hbo','S12_D29 hbo','S12_D32 hbo']

#plot dataframe results per subjects
for subjectlvli,DF in enumerate(glm_est_DF): 

    #prepare plot  
    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(20,10),gridspec_kw=dict(width_ratios=[1, 1]))
    fig.suptitle("Subject "+subjects[subjectlvli],fontsize='xx-large',weight='bold')
    plt.tight_layout()
    print("Subject "+subjects[subjectlvli])

    #get only motor related conditions
    DF=DF[DF["Condition"].str[-9:-1]=='Motortas']
    
    #find hbo and hbr min and max for display purposes
    maxhbo=DF[(DF["Chroma"]=='hbo')].theta.max()
    minhbo=DF[(DF["Chroma"]=='hbo')].theta.min()
    maxhbr=DF[(DF["Chroma"]=='hbr')].theta.max()
    minhbr=DF[(DF["Chroma"]=='hbr')].theta.min()
    
    #hbo and hbr colormap
    colorhbo=plt.cm.YlOrRd(np.linspace(0,1,num=11))
    colorhbr=plt.cm.GnBu_r(np.linspace(0,1,num=11))
    
        
    for badch in bad_channels[subjectlvli][0][0:-1:2]:         #gets bad channels from this subject session left 2s
        axes[0].scatter(coordinates._get_ch_pos()[badch[0:-3]+'hbo'][0],coordinates._get_ch_pos()[badch[0:-3]+'hbo'][1], marker="x", s=600, color='red', alpha=1)
    for badch in bad_channels[subjectlvli][1][0:-1:2]:         #gets bad channels from this subject session left 2s
        axes[1].scatter(coordinates._get_ch_pos()[badch[0:-3]+'hbo'][0],coordinates._get_ch_pos()[badch[0:-3]+'hbo'][1], marker="x", s=600, color='red', alpha=1)

    cleandata = pd.DataFrame(columns=['condition', 'ch_name', 'beta','p_value', 'corr_p_val','activated'])
    bonferroni = pd.DataFrame(columns=['condition', 'ch_name','rejected', 'corr_p_val'])
    i=0
    for cond in ['1-Motortask','2-Motortask']:
        condhbr=DF[(DF["Chroma"]=='hbr') & (DF["Condition"]==cond)]
        condhbo=DF[(DF["Chroma"]=='hbo') & (DF["Condition"]==cond)]
        rejectedhbr, corr_p_valhbr=mne.stats.fdr_correction(condhbr.p_value)
        rejectedhbo, corr_p_valhbo=mne.stats.fdr_correction(condhbo.p_value)
        for j in range(0,len(rejectedhbr)):
            bonferroni.loc[i]=[cond,condhbr.iloc[j].ch_name,rejectedhbr[j],corr_p_valhbr[j]]
            bonferroni.loc[i+1]=[cond,condhbo.iloc[j].ch_name,rejectedhbo[j],corr_p_valhbo[j]]
            i=i+2
    i=0
    for index in DF.index:
        bonferronisignifi=bool(bonferroni[(bonferroni.condition==DF.Condition[index]) & (bonferroni.ch_name==DF.ch_name[index])].rejected.values)
        bonferronipval=float(bonferroni[(bonferroni.condition==DF.Condition[index]) & (bonferroni.ch_name==DF.ch_name[index])].corr_p_val)
        if (DF.ch_name[index][-1]=='o' ): cleandata.loc[i]=([DF.Condition[index], DF.ch_name[index], DF.theta[index], DF.p_value[index], bonferronipval,(bonferronisignifi) and DF.theta[index]>0])
        else : cleandata.loc[i]=([DF.Condition[index], DF.ch_name[index], DF.theta[index], DF.p_value[index], bonferronipval,((bonferronisignifi) and (DF.theta[index]<0))])
        i=i+1
    
    #greendot=mpl.lines.Line2D([], [], color='slateblue', marker='o', linestyle='None', markersize=18, label='channels in Primary motor cortex', alpha=0.4)
    redot=mpl.lines.Line2D([], [], color=colorhbo[10], marker='o', linestyle='None', markersize=10, label='Activated channel', alpha=1)
    s3sup2s=mpl.lines.Line2D([], [], color=colorhbo[10], marker='o', linestyle='None', markersize=10, label='Beta HbO 3s > Beta HbO 2s', alpha=1)
    cross=mpl.lines.Line2D([], [], color='k', marker='x', linestyle='None', markersize=8, label='Not activated channel', alpha=1)
    QC=mpl.lines.Line2D([], [], color='red', marker='x', linestyle='None', markersize=10, label='Quality check fail', alpha=1)
    
    for i, title in enumerate(["Session 1","Session 2"]):
        axes[i].set(title=title, xlim=(-0.09,0.09), ylim=(-0.05,0.09), adjustable='box')
        axes[i].set_xticks([])
        axes[i].set_yticks([])
        axes[i].legend(handles=[redot, cross, QC], loc="lower center")
            
    righthbochname=cleandata.ch_name.drop_duplicates().to_list()[len(cleandata.ch_name.drop_duplicates().to_list()):-1:2]
    lefthbochname=cleandata.ch_name.drop_duplicates().to_list()[0:len(cleandata.ch_name.drop_duplicates().to_list()):2]
    
    cleandatas1=cleandata[cleandata.condition=='1-Motortask']
    cleandatas2=cleandata[cleandata.condition=='2-Motortask']
    
    print("Session 1")
    for chname in cleandata.ch_name.drop_duplicates().to_list()[0:len(cleandata.ch_name.drop_duplicates().to_list()):2]: 
        chcoord=coordinates._get_ch_pos()[chname]
        if(chname[-1]=='o'):
            hboline=cleandatas1[cleandatas1.ch_name==chname]
            hbrline=cleandatas1[cleandatas1.ch_name==chname[0:len(chname)-1]+'r']
            if (bool(hboline.activated.values) and bool(hbrline.activated.values) and not(np.isin(chname[0:-3]+'760',bad_channels[subjectlvli][0][0:-1:2]))):
                axes[0].scatter(chcoord[0],chcoord[1], s=200, color=colorhbo[10])
            else :
                axes[0].scatter(chcoord[0],chcoord[1], s=100, color='k', marker='x')
        else : print("An HbR channel name was encountered")


    print("Session 2")
    for chname in cleandata.ch_name.drop_duplicates().to_list()[0:len(cleandata.ch_name.drop_duplicates().to_list()):2]:
        chcoord=coordinates._get_ch_pos()[chname]
        hboline=cleandatas2[cleandatas2.ch_name==chname]
        hbrline=cleandatas2[cleandatas2.ch_name==chname[0:len(chname)-1]+'r']
        if (bool(hboline.activated.values) and bool(hbrline.activated.values) and not(np.isin(chname[0:-3]+'760',bad_channels[subjectlvli][0][0:-1:2]))):
            axes[1].scatter(chcoord[0],chcoord[1], s=200, color=colorhbo[10])
        else :
            axes[1].scatter(chcoord[0],chcoord[1], s=100, color='k', marker='x')


    fig.savefig(BidsPath+'/derivatives/FRESH-pipeline/Figure-sub_'+subjects[subjectlvli]+'.png')   
    plt.show()
