# -*- coding: utf-8 -*-
"""
Created on Mon Mar  7 15:37:51 2022
This script compare the impact of filtering before or after concentration changes
@author: nojegou
"""

import mne
import matplotlib.pyplot as plt
import numpy as np

raw_data=mne.io.read_raw_nirx("C:/Users/nojegou/Documents/monProjetIMANES/fNIRS/gr_01/su_06/2020-07-27_001",verbose=True, preload=True)
OD1=mne.preprocessing.nirs.optical_density(raw_data.copy())
Conc1=mne.preprocessing.nirs.beer_lambert_law(OD1,ppf=6)
BPfilt1=Conc1.copy().filter(0.01, 0.09, l_trans_bandwidth=0.009,h_trans_bandwidth=0.01, method='fir')


## filter cannot be before OD conversion because it creates unrelated signals
OD2=mne.preprocessing.nirs.optical_density(raw_data.copy())
BPfilt2=OD2.copy().filter(0.01, 0.09, l_trans_bandwidth=0.009,h_trans_bandwidth=0.01, method='fir')
Conc2=mne.preprocessing.nirs.beer_lambert_law(BPfilt2,ppf=6)

"""
Final signal comparison
"""
Gsig=[8,16]#4,6,8,10,12,16,18,22,24,26,28,30,36,38]
for sig in Gsig:
    plt.plot(BPfilt1.times,BPfilt1._data[sig])
    plt.plot(BPfilt1.times,BPfilt1._data[sig+1])
   # plt.vlines([BPfilt1.annotations.onset[0]], haut,bas ,colors="gold")
    plt.vlines([BPfilt1.annotations.onset[1]],max(BPfilt1._data[sig]),min(BPfilt1._data[sig]) ,colors="mediumaquamarine",linewidths=3)
    plt.vlines([BPfilt1.annotations.onset[2:6]], max(BPfilt1._data[sig]),min(BPfilt1._data[sig]) ,colors="mediumpurple",linewidths=3)
    plt.legend(["HbR - Filter post Conc","HbO - Filter post Conc"])
    plt.title("Conc Filtered",fontsize=30,  fontweight="bold")
    plt.xlim(BPfilt1.annotations.onset[1]-5,750)
    plt.show()
    
    plt.plot(Conc2.times,Conc2._data[sig])
    plt.plot(Conc2.times,Conc2._data[sig+1])
    plt.vlines([BPfilt1.annotations.onset[1]],max(Conc2._data[sig]),min(Conc2._data[sig]) ,colors="mediumaquamarine",linewidths=3)
    plt.vlines([BPfilt1.annotations.onset[2:6]], max(Conc2._data[sig]),min(Conc2._data[sig]) ,colors="mediumpurple",linewidths=3)
    plt.legend(["HbR - Filter pre OD","HbO - Filter pre OD"])    
    plt.title("Raw data Filtered",fontsize=30,  fontweight="bold")
    plt.xlim(BPfilt1.annotations.onset[1]-5,750)
    plt.show()
    
"""
Mean absolute difference
"""
DiffOut=np.abs(BPfilt1._data-Conc2._data)
plt.plot(np.mean(DiffOut,1))
plt.legend(["Moyenne de la différence des signaux de sortie"])#,"Moyenne de la différence des signaux de concentration HbR","Moyenne de la différence des signaux de concentration HbO","Moyenne de la différence des HbR filtrés","Moyenne de la différence des HbO filtrés"])
plt.title("Mean absolute difference")
plt.xlabel("Channel")
plt.show()    

