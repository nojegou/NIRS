# -*- coding: utf-8 -*-
"""
Created on Fri Nov  4 10:30:02 2022

@author: nojegou
"""

#--------Raw signal Observation---------------------------------------------------------------------------------------------------------------

import mne_bids
BidsPath="C:/Users/nojegou/Documents/Data/IMANES-BIDS-both-session-clean"
dataset = mne_bids.BIDSPath(session="1", root=BidsPath, task="motortask", datatype="nirs", suffix="nirs", extension=".snirf")
subjects = mne_bids.get_entity_vals(BidsPath, 'subject') #['01','04','05','06','07','08','09','10','11']

for sub in subjects:
    bids_path = dataset.update(subject=sub)
    raw=mne_bids.read_raw_bids(bids_path=bids_path, extra_params=None, verbose=True)
#    raw.plot(start=0,duration=raw.times[-1], n_channels=40, show=True)
    #raw.annotations.rename({'1.0': 'Resting state', '2.0': 'Introduction', '3.0': 'Motor task'})
    i=0
    RightTrig=False
    while ((i<len(raw.annotations.description)) and not(RightTrig)):
        RightTrig=(raw.annotations.description[i]=='Motortask')
        if (RightTrig):
            raw.crop(raw.annotations.onset[i]-27,raw.annotations.onset[i]+180)
            #mne_bids.write_raw_bids(raw, bids_path=bids_path, overwrite=True) 
        else : i=i+1
#    raw.plot(start=0,duration=raw.times[-1], n_channels=40, show=True)
     
"""
def readncrop(bids_path, raw):
    import mne_bids
    #raw=mne_bids.read_raw_bids(bids_path=bids_path, extra_params=None, verbose=True)
    i=0
    RightTrig=False
    while ((i<len(raw.annotations.description)) and not(RightTrig)):
        RightTrig=(raw.annotations.descri
        
import mne_bids  
BidsPath="C:/Users/nojegou/Documents/Data/IMANES-BIDS-both-session-clean"
dataset = mne_bids.BIDSPath(session="1", root=BidsPath, task="motortask", datatype="nirs", suffix="nirs", extension=".snirf")
subjects = mne_bids.get_entity_vals(BidsPath, 'subject') #['01','04','05','06','07','08','09','10','11']

for sub in subjects:
    bids_path = dataset.update(subject=sub)
    raw=mne_bids.read_raw_bids(bids_path=bids_path, extra_params=None, verbose=True)
    raw=readncrop(bids_path, raw)
    raw.plot()
    """