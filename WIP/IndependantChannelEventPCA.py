# -*- coding: utf-8 -*-
"""
Created on Mon Apr 11 14:01:48 2022
This is a basic pipeline where I tried the epoching and motion correction option from MNE
@author: nojegou
"""

import mne
import matplotlib.pyplot as plt
import numpy
import mne_nirs
from mne.decoding import UnsupervisedSpatialFilter
from sklearn.decomposition import PCA

### Import data
#raw_data=mne.io.read_raw_nirx("C:/Users/nojegou/Documents/monProjetIMANES/fNIRS/gr_01/su_06/2020-07-27_001",verbose=True, preload=True)
raw_data=mne.io.read_raw_nirx("C:/Users/nojegou/Documents/monProjetIMANES/fNIRS/gr_01/su_06/2020-07-27_002",verbose=True, preload=True)
raw_data.annotations.rename({'1.0': 'Resting state', '2.0': 'Introduction', '3.0': 'Motor task'})

#CropData=raw_data.copy().crop(raw_data.annotations.onset[2]-20,raw_data.times[len(raw_data.times)-1])

tstart = raw_data.annotations.onset[1]



OD=mne.preprocessing.nirs.optical_density(raw_data.copy())
OD._data=OD._data*1e9     #This line allow to have the same processing as MATLAB
Conc=mne.preprocessing.nirs.beer_lambert_law(OD,ppf=6*1000)

BPfilt=Conc.copy().filter(0.01, 0.09, l_trans_bandwidth=0.009,h_trans_bandwidth=0.01, method='fir')

###Plot raw mean
start=int(raw_data.annotations.onset[1]*BPfilt.info['sfreq'])-10
end=int(len(BPfilt._data[0])-1)
plt.plot(BPfilt.times[start:end],numpy.mean(BPfilt._data[0:40:2,start:end],axis=0))
plt.plot(BPfilt.times[start:end],numpy.mean(BPfilt._data[1:40:2,start:end],axis=0))
#plt.vlines([BPfilt.annotations.onset[0]], -0.20,0.2 ,colors="gold")
plt.vlines([BPfilt.annotations.onset[1]], -0.4,0.4 ,colors="gold")
plt.vlines([BPfilt.annotations.onset[2:6]], -0.4,0.4 ,colors="forestgreen")
plt.legend(["Mean HbO","Mean HbR","Introduction","Motor task"])
plt.show()


events, event_dict = mne.events_from_annotations(BPfilt)
epochs=mne.Epochs(BPfilt,events,event_dict, tmin=-10,tmax=45,baseline=(-10,0))
epochs['Motor task'].plot_image(combine='mean',ts_args=dict(ylim=dict(hbo=[-600000,600000],hbr=[-600000, 600000])))


pca = UnsupervisedSpatialFilter(PCA(30), average=False)
pca_data = pca.fit_transform(epochs.get_data())

ev = mne.EvokedArray(numpy.mean(pca_data, axis=0),
                     mne.create_info(30, epochs.info['sfreq'],
                                     ch_types='hbo'))
ev.plot(show=False, window_title="PCA", time_unit='s')

#GLM
"""
design_matrix = mne_nirs.experimental_design.make_first_level_design_matrix(raw_data, drift_model='cosine', high_pass=0.001, hrf_model='spm', stim_dur=18, drift_order=10)  
fig, ax1 = plt.subplots(figsize=(10, 6), nrows=1, ncols=1)
from nilearn.plotting import plot_design_matrix
fig = plot_design_matrix(design_matrix, ax=ax1)
glm_est = mne_nirs.statistics.run_glm(BPfilt, design_matrix,noise_model='auto')
glm_est.plot_topo(conditions=['Motor task'])
MotorTask=glm_est.to_dataframe()
MotorTask.drop(MotorTask[MotorTask['Condition']!='Motor task'].index, inplace=True)
"""