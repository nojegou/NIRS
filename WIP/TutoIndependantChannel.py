# -*- coding: utf-8 -*-
"""
Created on Wed Jan 19 11:05:54 2022
Trial of different filters
@author: nojegou
"""
import mne
import matplotlib.pyplot as plt
import numpy
import mne_nirs


### Import data
#raw_data=mne.io.read_raw_nirx("C:/Users/nojegou/Documents/monProjetIMANES/fNIRS/gr_01/su_06/2020-07-27_001",verbose=True, preload=True)
raw_data=mne.io.read_raw_nirx("C:/Users/nojegou/Documents/monProjetIMANES/fNIRS/gr_01/su_06/2020-07-27_002",verbose=True, preload=True)
raw_data.annotations.rename({'1.0': 'Resting state', '2.0': 'Introduction', '3.0': 'Motor task'})

#raw_data=raw_data.copy().crop(raw_data.annotations.onset[2]-20,raw_data.times[len(raw_data.times)-1])

tstart = raw_data.annotations.onset[1]

OD=mne.preprocessing.nirs.optical_density(raw_data.copy())
OD._data=OD._data*1e9  
Conc=mne.preprocessing.nirs.beer_lambert_law(OD,ppf=6*1000)

StartCh=0
EndCh=len(raw_data.ch_names)
'''
i=StartCh
while i <EndCh:#len(raw_data.ch_names)/2):
    plt.psd(raw_data._data[i], Fs=7.812500)  #Sampling rate ???
    i+=1
plt.xlim(0.15,3.8)
plt.title("PSD raw",fontsize=30,  fontweight="bold")
plt.legend(Conc.ch_names[StartCh:EndCh],bbox_to_anchor=[1,1], loc='upper left')
plt.show()

i=StartCh
legend=[]
while i <EndCh:#len(raw_data.ch_names)/2):
    plt.psd(Conc._data[i], Fs=7.812500)  #Sampling rate ???
    legend.append(Conc.ch_names[i])
    i+=2
#plt.xlim(0.15,3.8)
plt.title("PSD Concentration",fontsize=30,  fontweight="bold")
plt.legend(legend,bbox_to_anchor=[1,1], loc='upper left')
plt.show()
'''
#Conc.plot(n_channels=len(raw_data.ch_names), duration=200, show_scrollbars=True, scalings='auto', clipping=None, start=tstart)

### Lowpass filter
'''
LPfilt=Conc.copy().filter(None, 0.1,l_trans_bandwidth=0.2,h_trans_bandwidth=0.02)
LPfilt.plot(n_channels=len(LPfilt.ch_names), duration=200, show_scrollbars=True, scalings='auto', clipping=None, start=tstart)

i=StartCh
while i <EndCh:#len(raw_data.ch_names)/2):
     #Sampling rate ???
    plt.psd(LPfilt._data[i],Fs=7.812500,linestyle="--")
    i+=1
#plt.xlim(0,1)
plt.legend(LPfilt.ch_names[StartCh:EndCh],bbox_to_anchor=[1,1], loc='upper left')
plt.title("PSD Lowpass filtered concentration",fontsize=30,  fontweight="bold")
plt.show()

Dleg=[]
for el in Conc.ch_names:
    Dleg.append(el)
    Dleg.append(el)
plt.legend(Dleg[StartCh*2:EndCh*2],bbox_to_anchor=[1,1], loc='upper left')
'''
### Bandpass filter
        #old version BPfilt=Conc.copy().filter(0.01, 0.1, l_trans_bandwidth=0.001,h_trans_bandwidth=0.02)
BPfilt=Conc.copy().filter(0.01, 0.09, l_trans_bandwidth=0.009,h_trans_bandwidth=0.01, method='fir')
#BPfilt.plot(n_channels=len(BPfilt.ch_names), duration=200, show_scrollbars=True, scalings='auto', clipping=None, start=tstart)
#welchPSD,freqz=mne.time_frequency.psd_welch(BPfilt,tmin=0,tmax=730,fmin=0.5,fmax=4,n_fft=120,n_overlap=60,n_per_seg=120)
#BPfilt.annotations.rename({'1.0': 'Resting state', '2.0': 'Introduction', '3.0': 'Motor task'})
"""
i=StartCh
while i <EndCh:
    plt.psd(BPfilt._data[i],NFFT=120,noverlap=60,Fs=7.812500)#,color='grey')
    #plt.plot(freqz,welchPSD[i,:])
    i+=1
   
plt.title("PSD Bandpass filtered Concentration ",fontsize=30,  fontweight="bold")
plt.legend(BPfilt.ch_names[StartCh:EndCh],bbox_to_anchor=[1,1], loc='upper left')
        #plt.xlim(0,1)
plt.show()
        #plt.psd(numpy.mean(BPfilt._data,axis=0),Fs=7.812500)
        """
'''        
###Welch PSD
welchPSD,freqz=mne.time_frequency.psd_welch(BPfilt,tmin=0,tmax=730,fmin=0,fmax=4,n_fft=120,n_overlap=60,n_per_seg=120)
for i in range(1,len(welchPSD)):
    plt.plot(freqz,welchPSD[i,:])
   # plt.plot(welchPSD[i,:],freqz)
#plt.legend(["Welch BP PSD","BP PSD"],bbox_to_anchor=[1,1], loc='upper right')
plt.title("Welch PSD Bandpass Filtered Concentartion Changes")
plt.show()
'''

### PSD comparison
"""
plt.psd(numpy.mean(Conc._data,axis=0),NFFT=120,noverlap=60,Fs=7.812500) 
        #plt.psd(numpy.mean(LPfilt._data,axis=0))
plt.psd(numpy.mean(BPfilt._data,axis=0),NFFT=120,noverlap=60,Fs=7.812500)
plt.legend(["Concentration changes","Band pass filtered concentration changes"],bbox_to_anchor=[1,1], loc='upper right')#"Low pass filtered concentration changes",
plt.title("Power Spactral Density",fontsize=30,  fontweight="bold")
plt.show()

#Signal with a great Conc PSD
Gsig=[]#2,4,6,8,10,12,16,18,22,24,26,28,30,36,38]
for sig in Gsig:
    plt.plot(BPfilt.times,BPfilt._data[sig])
    plt.plot(BPfilt.times,BPfilt._data[sig+1])
    #plt.plot(LPfilt.times,LPfilt._data[sig])
    #plt.plot(LPfilt.times,LPfilt._data[sig+1])
    plt.vlines([BPfilt.annotations.onset[0]], 1.5e-6,-1.5e-6 ,colors="gold")
    plt.vlines([BPfilt.annotations.onset[1]], 1.5e-6,-1.5e-6 ,colors="royalblue")
    plt.vlines([BPfilt.annotations.onset[2:6]], 1.5e-6,-1.5e-6 ,colors="forestgreen")
    plt.legend(["HbO Bandpass signal","HbR Bandpass signal"])#♣, "HbO Lowpass signal","HbR Lowpass signal","HbO Bandpass signal","HbR Bandpass signal"
    plt.title(Conc.ch_names[sig]+" & "+Conc.ch_names[sig+1]+" channel with a Bandpass filter",fontsize=30,  fontweight="bold")
    plt.xlim(BPfilt.annotations.onset[1]-5,750)
    plt.show()
"""

